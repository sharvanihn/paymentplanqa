{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "cards": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "components": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "type": {
                  "type": "string"
                },
                "viewType": {
                  "type": "string"
                },
                "props": {
                  "type": "object",
                  "properties": {
                    "brand": {
                      "type": "object",
                      "properties": {
                        "uidx": {
                          "type": "string"
                        },
                        "name": {
                          "type": "string"
                        },
                        "image": {
                          "type": "string"
                        },
                        "bio": {
                          "type": "string"
                        },
                        "social": {
                          "type": "object",
                          "properties": {
                            "followersCount": {
                              "type": "integer"
                            },
                            "isFollowing": {
                              "type": "boolean"
                            },
                            "tags": {
                              "type": "string"
                            }
                          }
                        }
                      }
                    }
                  }
                },
                "args": {
                  "type": "object",
                  "properties": {
                    "show": {
                      "type": "boolean"
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}