{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "id": "http://jsonschema.net",
  "type": "object",
  "properties": {
    "data": {
      "id": "http://jsonschema.net/data",
      "type": "object",
      "properties": {
        "aspectRatio": {
          "id": "http://jsonschema.net/data/aspectRatio",
          "type": "string"
        },
        "authoredBy": {
          "id": "http://jsonschema.net/data/authoredBy",
          "type": "object",
          "properties": {
            "uidx": {
              "id": "http://jsonschema.net/data/authoredBy/uidx",
              "type": "string"
            }
          }
        },
        "authoredOn": {
          "id": "http://jsonschema.net/data/authoredOn",
          "type": "string"
        },
        "description": {
          "id": "http://jsonschema.net/data/description",
          "type": "string"
        },
        "id": {
          "id": "http://jsonschema.net/data/id",
          "type": "string"
        },
        "image": {
          "id": "http://jsonschema.net/data/image",
          "type": "string"
        },
        "modifiedBy": {
          "id": "http://jsonschema.net/data/modifiedBy",
          "type": "object",
          "properties": {
            "uidx": {
              "id": "http://jsonschema.net/data/modifiedBy/uidx",
              "type": "string"
            }
          }
        },
        "modifiedOn": {
          "id": "http://jsonschema.net/data/modifiedOn",
          "type": "string"
        },
        "productRack": {
          "id": "http://jsonschema.net/data/productRack",
          "type": "object",
          "properties": {
            "productIds": {
              "id": "http://jsonschema.net/data/productRack/productIds",
              "type": "array",
              "items": []
            }
          }
        },
        "publishedBy": {
          "id": "http://jsonschema.net/data/publishedBy",
          "type": "object",
          "properties": {
            "uidx": {
              "id": "http://jsonschema.net/data/publishedBy/uidx",
              "type": "string"
            }
          }
        },
        "publishedChannel": {
          "id": "http://jsonschema.net/data/publishedChannel",
          "type": "string"
        },
        "publishedOn": {
          "id": "http://jsonschema.net/data/publishedOn",
          "type": "string"
        },
        "source": {
          "id": "http://jsonschema.net/data/source",
          "type": "string"
        },
        "status": {
          "id": "http://jsonschema.net/data/status",
          "type": "string"
        },
        "storyId": {
          "id": "http://jsonschema.net/data/storyId",
          "type": "string"
        },
        "tags": {
          "id": "http://jsonschema.net/data/tags",
          "type": "array",
          "items": {
            "id": "http://jsonschema.net/data/tags/0",
            "type": "object",
            "properties": {
              "name": {
                "id": "http://jsonschema.net/data/tags/0/name",
                "type": "string"
              },
              "type": {
                "id": "http://jsonschema.net/data/tags/0/type",
                "type": "string"
              }
            }
          }
        },
        "target": {
          "id": "http://jsonschema.net/data/target",
          "type": "string"
        },
        "type": {
          "id": "http://jsonschema.net/data/type",
          "type": "string"
        },
        "url": {
          "id": "http://jsonschema.net/data/url",
          "type": "string"
        },
        "user": {
          "id": "http://jsonschema.net/data/user",
          "type": "object",
          "properties": {
            "uidx": {
              "id": "http://jsonschema.net/data/user/uidx",
              "type": "string"
            }
          }
        },
        "objectId": {
          "id": "http://jsonschema.net/data/objectId",
          "type": "string"
        }
      },
      "required": [
        "aspectRatio",
        "authoredBy",
        "authoredOn",
        "description",
        "id",
        "image",
        "modifiedBy",
        "modifiedOn",
        "productRack",
        "publishedBy",
        "publishedChannel",
        "publishedOn",
        "source",
        "status",
        "storyId",
        "tags",
        "target",
        "type",
        "url",
        "user",
        "objectId"
      ]
    },
    "meta": {
      "id": "http://jsonschema.net/meta",
      "type": "object",
      "properties": {
        "status": {
          "id": "http://jsonschema.net/meta/status",
          "type": "integer"
        }
      }
    }
  },
  "required": [
    "data",
    "meta"
  ]
}