{
	"type":"object",
	"$schema": "http://json-schema.org/draft-03/schema",
	"id": "http://jsonschema.net",
	"required":true,
	"properties":{
		"data": {
			"type":"object",
			"id": "http://jsonschema.net/data",
			"required":true,
			"properties":{
				"message": {
					"type":"string",
					"id": "http://jsonschema.net/data/message",
					"required":true
				},
				"success": {
					"type":"boolean",
					"id": "http://jsonschema.net/data/success",
					"required":true
				}
			}
		}
	}
}