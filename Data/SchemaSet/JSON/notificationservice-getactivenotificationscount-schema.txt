{
	"type":"object",
	"$schema": "http://json-schema.org/draft-03/schema",
	"id": "http://jsonschema.net",
	"required":true,
	"properties":{
		"actions": {
			"type":"object",
			"id": "http://jsonschema.net/actions",
			"required":true,
			"properties":{
				"getActiveNotifications": {
					"type":"string",
					"id": "http://jsonschema.net/actions/getActiveNotifications",
					"required":true
				}
			}
		},
		"numberOfActiveNotifications": {
			"type":"number",
			"id": "http://jsonschema.net/numberOfActiveNotifications",
			"required":true
		},
		"success": {
			"type":"boolean",
			"id": "http://jsonschema.net/success",
			"required":true
		}
	}
}