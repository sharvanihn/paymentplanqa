{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "id": "/",
  "type": "object",
  "properties": {
    "data": {
      "id": "data",
      "type": "array",
      "items": {
        "id": "auto-generated-schema-889"
      }
    },
    "count": {
      "id": "count",
      "type": "integer"
    }
  },
  "required": [
    "data",
    "count"
  ]
}