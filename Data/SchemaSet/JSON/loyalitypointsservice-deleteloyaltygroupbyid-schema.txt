{
	"type":"object",
	"$schema": "http://json-schema.org/draft-03/schema",
	"id": "http://jsonschema.net",
	"required":true,
	"properties":{
		"lpGroupResponseEntry": {
			"type":"object",
			"id": "http://jsonschema.net/lpGroupResponseEntry",
			"required":true,
			"properties":{
				"id": {
					"type":"number",
					"id": "http://jsonschema.net/lpGroupResponseEntry/id",
					"required":true
				}
			}
		},
		"status": {
			"type":"object",
			"id": "http://jsonschema.net/status",
			"required":true,
			"properties":{
				"statusCode": {
					"type":"number",
					"id": "http://jsonschema.net/status/statusCode",
					"required":true
				},
				"statusMessage": {
					"type":"string",
					"id": "http://jsonschema.net/status/statusMessage",
					"required":true
				},
				"statusType": {
					"type":"string",
					"id": "http://jsonschema.net/status/statusType",
					"required":true
				},
				"totalCount": {
					"type":"number",
					"id": "http://jsonschema.net/status/totalCount",
					"required":true
				}
			}
		}
	}
}