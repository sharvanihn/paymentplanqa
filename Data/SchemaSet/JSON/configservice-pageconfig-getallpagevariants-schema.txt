{
	"type":"object",
	"$schema": "http://json-schema.org/draft-03/schema",
	"id": "http://jsonschema.net",
	"required":true,
	"properties":{
		"data": {
			"type":"array",
			"id": "http://jsonschema.net/data",
			"required":true,
			"items":
			{
				"type":"object",
				"id": "http://jsonschema.net/data/0",
				"required":true,
				"properties":{
					"abtestName": {
						"type":"string",
						"id": "http://jsonschema.net/data/0/abtestName",
						"required":true
					},
					"abtestVariant": {
						"type":"string",
						"id": "http://jsonschema.net/data/0/abtestVariant",
						"required":true
					},
					"id": {
						"type":"number",
						"id": "http://jsonschema.net/data/0/id",
						"required":true
					},
					"userType": {
						"type":"number",
						"id": "http://jsonschema.net/data/0/userType",
						"required":true
					}
				}
			}
		},
		"status": {
			"type":"object",
			"id": "http://jsonschema.net/status",
			"required":true,
			"properties":{
				"statusCode": {
					"type":"number",
					"id": "http://jsonschema.net/status/statusCode",
					"required":true
				},
				"statusMessage": {
					"type":"string",
					"id": "http://jsonschema.net/status/statusMessage",
					"required":true
				},
				"statusType": {
					"type":"string",
					"id": "http://jsonschema.net/status/statusType",
					"required":true
				},
				"totalCount": {
					"type":"number",
					"id": "http://jsonschema.net/status/totalCount",
					"required":true
				}
			}
		}
	}
}