package com.myntra.pps.tests;


import com.myntra.pps.data.StyleData;
import com.myntra.pps.assist.PpsAssist;
import com.myntra.pps.Constants.PpsConstants;
import com.myntra.test.commons.testbase.BaseTest;
import org.testng.ITestContext;
import org.testng.annotations.BeforeMethod;

import java.util.ArrayList;
import java.util.List;

public class JabongPpsTests extends BaseTest{

    PpsAssist assist = new PpsAssist();

    @BeforeMethod(alwaysRun = true)
    public void setup(ITestContext context){
        StyleData style1 = new StyleData();
        style1.setStyleId("2610245");
        style1.setSkuId("16116360");

        List<StyleData> styles = new ArrayList<>();
        styles.add(style1);
        context.setAttribute("StyleData", styles);
        context.setAttribute("email", "mtj99@jabong.com");
        context.setAttribute("pswd", "123456");
    }

    //@Test(priority = 19)
    public void jabong_placeV2CodOrder(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String orderid = assist.createCodOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"),
                PpsConstants.JABONG_CLIENTID, PpsConstants.JABONG_STOREID,
                styles,  PpsConstants.JABONG_PPS_DATASOURCE, false, null, null);
        assist.cancelOrder(orderid, PpsConstants.JABONG_PPS_DATASOURCE, PpsConstants.JABONG_X_MYNT_CTX, false);
    }

    //@Test(priority = 20)
    public void placeV2OnlineOrder(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String orderid = assist.createOnlineOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"),
                PpsConstants.JABONG_CLIENTID, PpsConstants.JABONG_STOREID,
                styles,  PpsConstants.JABONG_PPS_DATASOURCE, false, null, null);
        assist.cancelOrder(orderid, PpsConstants.JABONG_PPS_DATASOURCE, PpsConstants.JABONG_X_MYNT_CTX, false);
    }

    //@Test
    public void placePaytmWalletOrderDirectDebit(){
        StyleData style1 = new StyleData();
        style1.setStyleId("2610245");
        style1.setSkuId("16116360");
        style1.setQty(1);
        List<StyleData> styles = new ArrayList<>();
        styles.add(style1);

        String orderid = assist.createWalletOrder("mtj99@jabong.com", "123456",
                PpsConstants.JABONG_CLIENTID, PpsConstants.JABONG_STOREID,
                styles,  "jabong_pps",false, null, null);
        assist.cancelOrder(orderid, "jabong_pps",
                PpsConstants.JABONG_X_MYNT_CTX, false);
    }

    //@Test
    public void placePaytmWalletOrderRedirect(){
        StyleData style1 = new StyleData();
        style1.setStyleId("2610245");
        style1.setSkuId("16116360");
        style1.setQty(1);
        List<StyleData> styles = new ArrayList<>();
        styles.add(style1);

        String orderid = assist.createWalletOrder("mtj99@jabong.com", "123456",
                PpsConstants.JABONG_CLIENTID, PpsConstants.JABONG_STOREID,
                styles, "jabong_pps", false, null, null);

    }
}
