package com.myntra.pps.tests;

import com.myntra.pps.data.StyleData;
import com.myntra.pps.assist.PpsAssist;
import com.myntra.pps.Constants.PpsConstants;
import com.myntra.test.commons.testbase.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class MyntraPpsV1Tests extends BaseTest{
    PpsAssist assist = new PpsAssist();

    @BeforeSuite
    public void usersetup(ITestContext context){
        context.setAttribute("gcNumber", System.getenv("gcNumber"));
        context.setAttribute("gcPin", System.getenv("gcPin"));
    }

    @BeforeMethod(alwaysRun = true)
    public void setup(ITestContext context, Method method){
        StyleData style1 = new StyleData();
        style1.setStyleId("1531");
        style1.setSkuId("3833");
        style1.setQty(1);
        StyleData style2 = new StyleData();
        style2.setStyleId("1533");
        style2.setSkuId("3841");
        style2.setQty(2);

        List<StyleData> styles = new ArrayList<>();
        styles.add(style1);
        styles.add(style2);
        context.setAttribute("StyleData", styles);
        context.setAttribute("email", "autotestv1@myntra.com");
        context.setAttribute("pswd", "123456");
        log.debug("\n\n#############  " + method.getName() + "##############\n\n");
        }


    @Test(priority = 1, groups = { "V1", "Regression", "agc", "Cancel"})
    public void lpAgcMyntIcbOnlineOrder_Cancel(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String orderid = assist.createLpAgcMyntIcbOnlineOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, true, null, null);
        Reporter.log(orderid);
        assist.cancelOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, true);
    }

    @Test(priority = 2, groups = { "V1", "Regression", "Cancel"})
    public void codOrder_Cancel(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String orderid = assist.createCodOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles, PpsConstants.MYNTRA_PPS_DATASOURCE, true, null, null);
        Reporter.log(orderid);
        assist.cancelOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, true);
        }

    @Test(priority = 3, groups = { "V1", "Regression", "Cancel"})
    public void onlineOrder_Cancel(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String orderid = assist.createOnlineOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, true, null, null);
        Reporter.log(orderid);
        assist.cancelOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, true);
    }

    @Test(priority = 4, groups = { "V1", "Regression", "mgc", "Cancel"})
    public void mgcAndCodOrder_Cancel(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String gcNumber = PpsConstants.gcNumber;
        String gcPin = PpsConstants.gcPin;
        if(null!=context.getAttribute("gcNumber") && null!=context.getAttribute("gcPin")){
            gcNumber = (String) context.getAttribute("gcNumber");
            gcPin = (String) context.getAttribute("gcPin");
        }
        String orderid = assist.createMgcAndCodOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, true, gcNumber, gcPin);
        Reporter.log(orderid);
        assist.cancelOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, true);
    }

    @Test(priority = 5, groups = { "V1", "Regression", "agc", "Return"})
    public void lpAgcMyntIcbOnlineOrder_Return(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String orderid = assist.createLpAgcMyntIcbOnlineOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, true, null, null);
        Reporter.log(orderid);
        assist.returnOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, true);
    }

    @Test(priority = 6, groups = { "V1", "Regression", "mgc", "Return"})
    public void mgcAndOnilneOrder_Return(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String gcNumber = PpsConstants.gcNumber;
        String gcPin = PpsConstants.gcPin;
        if(null!=context.getAttribute("gcNumber") && null!=context.getAttribute("gcPin")){
            gcNumber = (String) context.getAttribute("gcNumber");
            gcPin = (String) context.getAttribute("gcPin");
        }
        String orderid = assist.createMgcAndOnlineOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, true, gcNumber, gcPin);
        Reporter.log(orderid);
        assist.returnOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, true);
    }

    @Test(priority = 7, groups = { "V1", "Regression", "agc", "Exchange"})
    public void lpAgcMyntIcbOnlineOrder_Exchange(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        assist.createLpAgcMyntIcbOnlineOrder_exchange((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, true,
                PpsConstants.MYNTRA_OMS_DATASOURCE, null, null);
    }

    @Test(priority = 8, groups = { "V1", "Regression", "agc", "Cancel"})
    public void completeAgc_Cancel(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String orderid = assist.createCompleteAgcOrder("autotest_agc@myntra.com"
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, true, null, null);
        Reporter.log(orderid);
        assist.cancelOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, true);
    }

    @Test(priority = 9, groups = { "V1", "Regression", "mgc", "Cancel"})
    public void completeMgc_Cancel(ITestContext context){
        StyleData style1 = new StyleData();
        style1.setStyleId("2173958");
        style1.setSkuId("14440577");
        style1.setQty(1);
        List<StyleData> styles = new ArrayList<>();
        styles.add(style1);
        String gcNumber = PpsConstants.gcNumber;
        String gcPin = PpsConstants.gcPin;
        if(null!=context.getAttribute("gcNumber") && null!=context.getAttribute("gcPin")){
            gcNumber = (String) context.getAttribute("gcNumber");
            gcPin = (String) context.getAttribute("gcPin");
        }
        String orderid = assist.createCompleteMgcOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, true,gcNumber,gcPin);
        Reporter.log(orderid);
        assist.cancelOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, true);
    }
}
