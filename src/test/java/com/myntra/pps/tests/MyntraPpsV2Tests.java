package com.myntra.pps.tests;


import com.myntra.pps.data.StyleData;
import com.myntra.pps.assist.PpsAssist;
import com.myntra.pps.Constants.PpsConstants;
import com.myntra.test.commons.testbase.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class MyntraPpsV2Tests extends BaseTest{

    PpsAssist assist = new PpsAssist();

    @BeforeMethod(alwaysRun = true)
    public void setup(ITestContext context, Method method){
        log.debug("\n\n#############  " + method.getName() + "##############\n\n");
        StyleData style1 = new StyleData();
        style1.setStyleId("1531");
        style1.setSkuId("3833");
        style1.setQty(2);
        StyleData style2 = new StyleData();
        style2.setStyleId("1533");
        style2.setSkuId("3841");
        style2.setQty(1);

        List<StyleData> styles = new ArrayList<>();
        styles.add(style1);
        styles.add(style2);
        context.setAttribute("StyleData", styles);
        context.setAttribute("email", "autotestv2@myntra.com");
        context.setAttribute("pswd", "123456");
    }

    @Test(priority = 1, groups = { "V2", "Regression", "Cancel"})
    public void codV2Order_Cancel(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String orderid = assist.createCodOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"),
                PpsConstants.MYNTRA_CLIENTID, PpsConstants.MYNTRA_STOREID,
                styles,  PpsConstants.MYNTRA_PPS_DATASOURCE, false, null, null);
        Reporter.log(orderid);
        assist.cancelOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, false);
    }

    @Test(priority = 2, groups = { "V2", "Regression", "Cancel"})
    public void onlineV2Order_Cancel(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String orderid = assist.createOnlineOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"),
                PpsConstants.MYNTRA_CLIENTID, PpsConstants.MYNTRA_STOREID,
                styles,  PpsConstants.MYNTRA_PPS_DATASOURCE, false, null, null);
        Reporter.log(orderid);
        assist.cancelOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, false);
    }

    @Test(priority = 3, groups = { "V2", "Regression", "agc", "Cancel"})
    public void lpAgcMyntIcbOnlineV2Order_Cancel(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String orderid = assist.createLpAgcMyntIcbOnlineOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, false, null, null);
        Reporter.log(orderid);
        assist.cancelOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, false);
    }

    @Test(priority = 4, groups = { "V2", "Regression", "mgc", "Cancel"})
    public void mgcAndCodV2Order_Cancel(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String gcNumber = PpsConstants.gcNumber;
        String gcPin = PpsConstants.gcPin;
        if(null!=context.getAttribute("gcNumber") && null!=context.getAttribute("gcPin")){
            gcNumber = (String) context.getAttribute("gcNumber");
            gcPin = (String) context.getAttribute("gcPin");
        }
        String orderid = assist.createMgcAndCodOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, false, gcNumber, gcPin);
        Reporter.log(orderid);
        assist.cancelOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, false);
    }

    @Test(priority = 5, groups = { "V2", "Regression", "agc", "Return"})
    public void lpAgcMyntIcbOnlineV2Order_Return(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String orderid = assist.createLpAgcMyntIcbOnlineOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, false, null, null);
        Reporter.log(orderid);
        assist.returnOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, false);
    }

    @Test(priority = 6, groups = { "V2", "Regression", "mgc", "Return"})
    public void mgcAndOnlineV2Order_Return(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String gcNumber = PpsConstants.gcNumber;
        String gcPin = PpsConstants.gcPin;
        if(null!=context.getAttribute("gcNumber") && null!=context.getAttribute("gcPin")){
            gcNumber = (String) context.getAttribute("gcNumber");
            gcPin = (String) context.getAttribute("gcPin");
        }
        String orderid = assist.createMgcAndOnlineOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, false, gcNumber, gcPin);
        Reporter.log(orderid);
        assist.returnOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, false);
    }

    @Test(priority = 7, groups = { "V2", "Regression", "agc", "Exchange"})
    public void lpAgcMyntIcbOnlineV2Order_Exchange(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        assist.createLpAgcMyntIcbOnlineOrder_exchange((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, false,
                PpsConstants.MYNTRA_OMS_DATASOURCE, null, null);
    }

    @Test(priority = 8, groups = { "V2", "Regression", "agc", "Cancel"})
    public void completeAgcV2_Cancel(ITestContext context){
        List<StyleData> styles = (List<StyleData>) context.getAttribute("StyleData");
        String orderid = assist.createCompleteAgcOrder("autotest_agc@myntra.com"
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, false, null, null);
        Reporter.log(orderid);
        assist.cancelOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, false);
    }

    @Test(priority = 9, groups = { "V2", "Regression", "mgc", "Cancel"})
    public void completeMgcV2_Cancel(ITestContext context){
        StyleData style1 = new StyleData();
        style1.setStyleId("2173958");
        style1.setSkuId("14440577");
        style1.setQty(1);
        List<StyleData> styles = new ArrayList<>();
        styles.add(style1);
        String gcNumber = PpsConstants.gcNumber;
        String gcPin = PpsConstants.gcPin;
        if(null!=context.getAttribute("gcNumber") && null!=context.getAttribute("gcPin")){
            gcNumber = (String) context.getAttribute("gcNumber");
            gcPin = (String) context.getAttribute("gcPin");
        }
        String orderid = assist.createCompleteMgcOrder((String) context.getAttribute("email")
                , (String) context.getAttribute("pswd"), PpsConstants.MYNTRA_CLIENTID,
                PpsConstants.MYNTRA_STOREID, styles,PpsConstants.MYNTRA_PPS_DATASOURCE, false, gcNumber, gcPin);
        Reporter.log(orderid);
        assist.cancelOrder(orderid, PpsConstants.MYNTRA_PPS_DATASOURCE, PpsConstants.MYNTRA_X_MYNT_CTX, false);
    }
}