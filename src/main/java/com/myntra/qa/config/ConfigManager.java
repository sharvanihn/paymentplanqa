package com.myntra.qa.config;

import com.google.common.base.Strings;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Generic class to load Yaml config and and load in Map<String, Object> (Object can be a String or again be a Map)
 * It's Singleton
 */
public class ConfigManager {

    private static ConfigManager instance = null;
    private Properties configs = new Properties();
    private static final String ENV = "env";

    private ConfigManager() {
    }

    private static void loadConfigFile(InputStream inputStream) {

        instance = new ConfigManager();
        try {
            instance.configs.load(inputStream);
            String env = System.getenv(ENV) != null ? System.getenv(ENV) :
                    System.getProperty(ENV) != null ? System.getProperty(ENV) : instance.configs.getProperty(ENV);
            if (!Strings.isNullOrEmpty(env)) {
                InputStream envSpecificStream = ConfigManager.class.getClassLoader().
                        getResourceAsStream(env + ".properties");
                if (null == envSpecificStream) {
                    throw new RuntimeException("Add config file for env: " + env);
                }
                instance.configs.load(envSpecificStream);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static ConfigManager getInstance() {
        return getInstance(getConfigLocation());
    }

    private static ConfigManager getInstance(InputStream inputStream) {
        if (null == instance)
            synchronized (ConfigManager.class) {
                if (null == instance)
                    loadConfigFile(inputStream);
            }
        return instance;
    }

    private static InputStream getConfigLocation() {
        InputStream resource = ConfigManager.class.getClassLoader().getResourceAsStream("test-env.properties");
        if (null == resource)
            throw new RuntimeException("test-env.properties is not available in resources");
        return resource;
    }

    public String getConfig(String key) {
        return String.valueOf(configs.get(key));
    }

    public String getConfig(String key, String defaultValue) {
        return String.valueOf(null != configs.get(key) ? configs.get(key) : defaultValue);
    }

    public static void main(String[] args) throws Exception {
        ConfigManager configManager = getInstance();
        String h = configManager.getConfig("db.port");
    }




}
