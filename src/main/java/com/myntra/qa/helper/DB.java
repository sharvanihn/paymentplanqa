package com.myntra.qa.helper;



import com.myntra.qa.config.ConfigManager;

import java.util.Properties;

public enum DB {
    CASHBACK("cashBack_db"),
    CASHBACK_JABONG("cashBack_jabong_db");


    public final String nameKey;
    public final String user;
    public final String password;
    private final String name;
    private final String host;
    private final String port;

    DB(String dbName) {
        this.nameKey = dbName;
        ConfigManager configManager = ConfigManager.getInstance();
        String base = "db." + dbName;
        String key = "db." + dbName + ".name"; //db.budget.name
        this.name = configManager.getConfig(key);
        key = base + ".host";
        this.host = System.getenv(key) != null ? System.getenv(key) :
                configManager.getConfig(key, configManager.getConfig("db.host"));
        key = base + ".port";
        this.port = System.getenv(key) != null ? System.getenv(key) :
                configManager.getConfig(key, configManager.getConfig("db.port", "5499"));
        key = base + ".user";
        this.user = System.getenv(key) != null ? System.getenv(key) :
                configManager.getConfig(key, configManager.getConfig("db.user"));
        key = base + ".password";
        this.password = System.getenv(key) != null ? System.getenv(key) :
                configManager.getConfig(key, configManager.getConfig("db.password"));
    }

    public String getDbURL() {
        return "jdbc:mysql://" + host + ":" + port + "/" + name;
    }

    public String getName(Properties properties) {
        String base = "db." + this.nameKey;
        String key = base + ".name"; //db.budget.name
        return properties.getProperty(key);
    }

    public String getHost(Properties properties) {
        String base = "db." + this.nameKey;
        String key = base + ".host"; //db.budget.name
        return properties.getProperty(key) != null ? properties.getProperty(key) : properties.getProperty("db.host");
    }

    public String getPort(Properties properties) {
        String base = "db." + this.nameKey;
        String key = base + ".port"; //db.budget.name
        return properties.getProperty(key) != null ? properties.getProperty(key) : properties.getProperty("db.port");
    }
}
