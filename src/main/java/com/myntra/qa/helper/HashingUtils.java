package com.myntra.qa.helper;


import org.apache.commons.codec.binary.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashingUtils {

    private static final String CHECKSUM_DELIMITER = "|";

    private static final String HASH_TYPE = "SHA-256";

    public static String computeSHA256(String data) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(HASH_TYPE);
        byte[] hashBytes = md.digest(data.getBytes());
        return Hex.encodeHexString(hashBytes);
    }

    public String generateSignature(String objectString, String key) throws NoSuchAlgorithmException {
        try {
            return computeSHA256(objectString + CHECKSUM_DELIMITER + key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}