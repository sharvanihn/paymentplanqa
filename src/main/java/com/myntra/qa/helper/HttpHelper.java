package com.myntra.qa.helper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by arun on 13/2/17.
 * <p>
 * This Class contains utility methods for making http post and get sync calls.
 * Support for async call will added soon
 */

@Slf4j
public class HttpHelper {

    @Data
    @AllArgsConstructor
    @Slf4j
    public static class HttpResponse {
        HttpURLConnection con;
        String resp;
        int respCode;
    }

    public static HttpResponse get(String url, Map<String, String> headerMap) throws Exception {
        return makeHttpCall(url, "GET", headerMap);
    }

    public static HttpResponse get(String url) throws Exception {
        return makeHttpCall(url, "GET", null);
    }

    public static HttpResponse post(String url, Map<String, String> headerMap) throws Exception {
        return makeHttpCall(url, "POST", headerMap);
    }

    public static HttpResponse post(String url) throws Exception {
        return makeHttpCall(url, "POST", null);
    }

    private static HttpResponse makeHttpCall(String url, String method, Map<String, String> headerMap) throws Exception {
        log.debug("Http Call: {}", url);
        HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
        con.setRequestMethod(method);

        if (!(headerMap == null)) {
            con.setInstanceFollowRedirects(false);
            for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                //URLCONNECTION HEADERS ARE CASE-SENSITIVE.s
                con.addRequestProperty(entry.getKey(), entry.getValue());
            }
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return new HttpResponse(con, response.toString(), con.getResponseCode());
    }

    public static HttpResponse postJsonObject(String url, String content, Map<String, String> headerMap) throws Exception {
        return postObject(url, content, headerMap);
    }

    public static HttpResponse postObject(String url, String content, Map<String, String> headerMap) throws Exception {
        HttpClient client = new DefaultHttpClient();
        org.apache.http.HttpResponse response;
        HttpPost post = new HttpPost(url);

        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            post.addHeader(entry.getKey(), entry.getValue());
        }


        if (null != content) {
            StringEntity se = new StringEntity(content);
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, MediaType.APPLICATION_JSON));
            post.setEntity(se);
        }

        response = client.execute(post);
        int respCode = response.getStatusLine().getStatusCode();
        if (response != null) {
            InputStream in = response.getEntity().getContent();
            String responeString = IOUtils.toString(in);

            return new HttpResponse(null, responeString, respCode);
        }
        return null;
    }

    public static void main(String[] args) throws Exception {
        fun();

    }


    public static void fun() throws Exception {


        //  http://bhashsms.com/api/sendmsg.php?user=gulshan.karmani@gmai&pass=********&sender=Sender ID&phone=Mobile No&text=Test SMS&priority=Priority&stype=smstype

        String url = "http://bhashsms.com/api/sendmsg.php";

        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);

        // add header

        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("user", "gulshan.karmani@gmai"));
        urlParameters.add(new BasicNameValuePair("pass", "123456"));
        urlParameters.add(new BasicNameValuePair("sender", "INDSSM"));
        urlParameters.add(new BasicNameValuePair("phone", "8285411741"));
        urlParameters.add(new BasicNameValuePair("text", "hello"));
        urlParameters.add(new BasicNameValuePair("phone", "8285411741"));
        urlParameters.add(new BasicNameValuePair("priority", "ndnd"));
        urlParameters.add(new BasicNameValuePair("stype", "normal"));


        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        org.apache.http.HttpResponse response = client.execute(post);
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + post.getEntity());
        System.out.println("Response Code : " +
                response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        System.out.println(result.toString());


    }

}


