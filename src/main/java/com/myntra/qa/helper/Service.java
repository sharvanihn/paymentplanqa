package com.myntra.qa.helper;


import com.myntra.qa.config.ConfigManager;

public enum Service {
    PPS("pps");


    public String serviceName;

    public static final String CLUSTER_NAME = "clusterName";
    private static final String ENV = "env";

    Service(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceHost() {
        ConfigManager configManager = ConfigManager.getInstance();

        String env = System.getenv(ENV) != null ? System.getenv(ENV) :
                System.getProperty(ENV) != null ? System.getProperty(ENV) : configManager.getConfig(ENV);

        String host = configManager.getConfig(serviceName + ".host");
        if (env.equalsIgnoreCase("cluster")) {
            String clusterName = System.getenv(CLUSTER_NAME) != null ? System.getenv(CLUSTER_NAME) :
                    System.getProperty(CLUSTER_NAME) != null ? System.getProperty(CLUSTER_NAME) : configManager.getConfig("clusterName");
            host = clusterName + "-" + host;
        }
        return host;
    }


    public static void main(String[] args) {

        System.out.println(Service.PPS.getServiceHost());
    }

}


/*

Db Managed by DBA team
backup and recovery for appachhi plateform
 */