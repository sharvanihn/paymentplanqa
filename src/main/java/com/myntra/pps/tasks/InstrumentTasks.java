package com.myntra.pps.tasks;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.myntra.giftcard.domain.entry.GiftCardRequestEntry;
import com.myntra.giftcard.purchase.domain.entry.GiftCardPurchaseRequestEntry;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.gandalf.MyntraService;
import com.myntra.lordoftherings.gandalf.PayloadType;
import com.myntra.lordoftherings.gandalf.RequestGenerator;
import com.myntra.monk.core.XMetaRequestContext;
import com.myntra.plutus.domain.request.EligibilityRequest;
import com.myntra.pps.Constants.PpsConstants;
import com.myntra.pps.apiTests.APINAME;
import com.myntra.pps.apiTests.Myntra;
import com.myntra.pps.apiTests.ServiceType;
import com.myntra.pps.util.AuthUtil;
import com.myntra.pps.util.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.SkipException;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.UUID;

import static com.jayway.restassured.RestAssured.given;


public class InstrumentTasks {
    static Initialize init = new Initialize("/Data/configuration");
    private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentTasks.class);
    ObjectMapper mapper = new ObjectMapper();

    public String icbEligibility(String xMyntCtx, String amount, String cardNum, String cartId, String uidx){
        EligibilityRequest eligibilityRequest = new EligibilityRequest();
        eligibilityRequest.setAmount(Math.round(Double.valueOf(amount))*100);
        eligibilityRequest.setCardNumber(cardNum);
        eligibilityRequest.setCartId(cartId);
        eligibilityRequest.setLogin(uidx);

        HashMap<String, String> header = new HashMap<>();
        header.put(PpsConstants.CLIENT_HEADER, PpsConstants.CLIENT);
        header.put(PpsConstants.VERSION_HEADER, PpsConstants.VERSION);
        header.put(PpsConstants.X_META_CTX_HEADER, xMyntCtx);
        try {
            header.put(PpsConstants.AUTHORISATION_HEADER, AuthUtil.getAuthorizationHeader(eligibilityRequest, PpsConstants.CLIENT, PpsConstants.VERSION));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        MyntraService service = Myntra.getService(ServiceType.PORTAL_PLUTUS,
                APINAME.ELIGIBILITY, init.Configurations,
                new String[] {cardNum}, PayloadType.JSON, PayloadType.JSON);
        Response response = null;
        try {
            response = given().contentType(ContentType.JSON).headers(header)
                    .body(mapper.writeValueAsString(eligibilityRequest)).when().post(service.URL);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        String eligibleAmount = "NO";
        if("SUCCESS".equalsIgnoreCase(JsonPath.read(response.asString(), "$.status.statusType").toString())){
            eligibleAmount = JsonPath.read(response.asString(), "$.data.cashbackAmount").toString();
            eligibleAmount = String.valueOf(Math.round(Double.valueOf(eligibleAmount))/100);
        }
        return eligibleAmount;
    }

    //LoyaltyPoints

    public String getLoyaltyPoints(String xMyntCtx) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(XMetaRequestContext.X_META_CTX_HEADER, xMyntCtx);
        headers.put(PpsConstants.CLIENT_HEADER,PpsConstants.CLIENT);
        headers.put(PpsConstants.VERSION_HEADER, PpsConstants.VERSION);
        MyntraService service = Myntra.getService(ServiceType.PORTAL_LOYALITY,
                APINAME.ACCOUNTINFOLOYALITYV2, init.Configurations);
        RequestGenerator getLoyaltyPointsReqGen = new RequestGenerator(service, headers);
        String getLoyaltyPointsResponse = getLoyaltyPointsReqGen.respvalidate.returnresponseasstring();
        String availableLoyaltyPoints = JsonPath.read(getLoyaltyPointsResponse, "$.totalActivePoints").
                toString().replace("[", "")
                .replace("]", "").trim();
        return availableLoyaltyPoints;
    }

    static RequestGenerator creditLoyaltyPointsV2(String uidx, String xMyntCtx) {
        String points = "5000";
        String businessProcess = "GOOD_WILL";
        String expiryDate = "1592804888000";
        String description = "promotion credit";

        String keyval = PpsConstants.CLIENT+"|"+PpsConstants.VERSION+"|" + points + "|" + expiryDate + "|" + uidx + "|" +
                businessProcess + "|qa";
        String hashkey = org.apache.commons.codec.digest.DigestUtils.sha256Hex(keyval);

        MyntraService service = Myntra.getService(ServiceType.PORTAL_LOYALITY,
                APINAME.CREDITLOYALITYPOINTSV2, init.Configurations,
                new String[]{uidx, points, businessProcess, expiryDate, description}, PayloadType.JSON,
                PayloadType.JSON);
        HashMap<String, String> header = new HashMap<String, String>();
        header.put(XMetaRequestContext.X_META_CTX_HEADER, xMyntCtx);
        header.put(PpsConstants.CLIENT_HEADER,PpsConstants.CLIENT);
        header.put(PpsConstants.VERSION_HEADER, PpsConstants.VERSION);
        header.put(PpsConstants.AUTHORISATION_HEADER, hashkey);
        RequestGenerator req = new RequestGenerator(service, header);
        return req;
    }

    public void checkLoyaltyPoints(String xMyntCtx, String uidx) {
        String availableLoyaltyPoints = getLoyaltyPoints(xMyntCtx);
        if(Integer.valueOf(availableLoyaltyPoints)<=0){
            creditLoyaltyPointsV2(uidx, xMyntCtx);
        }
        availableLoyaltyPoints = getLoyaltyPoints(xMyntCtx);
        Assert.assertTrue(Integer.valueOf(availableLoyaltyPoints)>0,
                "Loyalty points are zero\n");
    }


    //Giftcard

    public String matchGiftCardBalance(String xMyntCtx, String uidx, Long amount){
        Long balance = getGiftcardUserBalance(xMyntCtx, uidx);
        if(amount>balance){
            purchaseGiftcardGoodWill(xMyntCtx, uidx, amount-balance+1);
        }
        balance = getGiftcardUserBalance(xMyntCtx, uidx);
        Assert.assertTrue(balance > 0, "Auto GC amount is zero\n");
        return balance.toString();
    }

    public Long getGiftcardUserBalance(String xMyntCtx, String uidx){
        HashMap<String, String> headers = new HashMap<>();
        headers.put(XMetaRequestContext.X_META_CTX_HEADER, xMyntCtx);
        headers.put(PpsConstants.CLIENT_HEADER,PpsConstants.CLIENT);
        headers.put(PpsConstants.VERSION_HEADER, PpsConstants.VERSION);
        MyntraService userBalanceService = Myntra.getService(ServiceType.PORTAL_GIFTCARDNEWSERVICE,
                APINAME.GETGIFTCARDADDEDTOUSERACCOUNTV2, init.Configurations,
                PayloadType.JSON, new String[] { uidx }, PayloadType.JSON);
        RequestGenerator userbalanceReqGen = new RequestGenerator(userBalanceService, headers);
        Assert.assertEquals(userbalanceReqGen.response.getStatus(), 200,
                "Giftcard user balance API is not working\n");
        String userbalanceResponse = userbalanceReqGen.respvalidate.returnresponseasstring();
        String availableGCBalance = JsonPath.read(userbalanceResponse, "$.data.totalBalance").toString().replace("[", "")
                .replace("]", "").trim();
        return Math.round(Double.valueOf(availableGCBalance));
    }

    public void purchaseGiftcardGoodWill(String xMyntCtx, String uidx, Long amount){
        GiftCardPurchaseRequestEntry GCpurchaseRequest = new GiftCardPurchaseRequestEntry();
        GCpurchaseRequest.setLogin(uidx);
        GCpurchaseRequest.setCardProgramName("Myntra Goodwill eGift Cards");
        GCpurchaseRequest.setAmount(amount*100);
        GCpurchaseRequest.setClientId(UUID.randomUUID().toString());
        String hashkey = "";
        try {
            hashkey = AuthUtil.getAuthorizationHeader(GCpurchaseRequest, PpsConstants.CLIENT, PpsConstants.VERSION);
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        HashMap<String, String> headers = new HashMap<>();
        headers.put(XMetaRequestContext.X_META_CTX_HEADER, xMyntCtx);
        headers.put(PpsConstants.CLIENT_HEADER,PpsConstants.CLIENT);
        headers.put(PpsConstants.VERSION_HEADER, PpsConstants.VERSION);
        headers.put(PpsConstants.AUTHORISATION_HEADER, hashkey);
        MyntraService goodwillService = Myntra.getService(ServiceType.PORTAL_GIFTCARDNEWSERVICE,
                APINAME.GOODWILLPURCHASE, init.Configurations,
                new String[] { GCpurchaseRequest.getLogin(), GCpurchaseRequest.getCardProgramName(),
                        GCpurchaseRequest.getAmount().toString(), GCpurchaseRequest.getClientId()});
        RequestGenerator goodWillReqgen = new RequestGenerator(goodwillService, headers);
        Assert.assertEquals(goodWillReqgen.response.getStatus(), 200,
                "Giftcard goodwill purchase API is not working\n");
        TimeUtil.explicitWait(PpsConstants.WAIT_TIME_FOR_GOODWILLPURCHASE); //This is required for the account to get updated with giftcard purchase
    }


    // giftcard balance

    public String getGiftcardBalance(String xMyntCtx, String uidx, String gcNumber, String gcPin){
        GiftCardRequestEntry giftCardRequestEntry = new GiftCardRequestEntry();
        giftCardRequestEntry.setCardNumber(gcNumber);
        giftCardRequestEntry.setPin(gcPin);
        giftCardRequestEntry.setLogin(uidx);
        giftCardRequestEntry.setType(PpsConstants.gcType);
        String hashkey = "";
        try {
            hashkey = AuthUtil.getAuthorizationHeader(giftCardRequestEntry, PpsConstants.CLIENT, PpsConstants.VERSION);
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        HashMap<String, String> headers = new HashMap<>();
        headers.put(XMetaRequestContext.X_META_CTX_HEADER, xMyntCtx);
        headers.put(PpsConstants.CLIENT_HEADER,PpsConstants.CLIENT);
        headers.put(PpsConstants.VERSION_HEADER, PpsConstants.VERSION);
        headers.put(PpsConstants.AUTHORISATION_HEADER, hashkey);
        MyntraService balanceService = Myntra.getService(ServiceType.PORTAL_GIFTCARDNEWSERVICE,
                APINAME.GIFTCARDNEWBALANCEV2, init.Configurations, new String[] { giftCardRequestEntry.getCardNumber(),
                        giftCardRequestEntry.getPin(), giftCardRequestEntry.getLogin(),
                        giftCardRequestEntry.getType()});
        RequestGenerator balanceReqGen = new RequestGenerator(balanceService, headers);
        Assert.assertEquals(balanceReqGen.response.getStatus(), 200,
                "Giftcard balance API is not working\n");
        String availableGCBalance = JsonPath.read(balanceReqGen.respvalidate.returnresponseasstring(),
                "$.data.amount").toString().replace("[", "")
                .replace("]", "").trim();
        if(Math.round(Double.valueOf(availableGCBalance))<1){
        throw new SkipException("Skipping test as the giftcard amount is 0 for gcNumber: " + gcNumber + " gcPin: " + gcPin);
        }
        return availableGCBalance;
    }

}
