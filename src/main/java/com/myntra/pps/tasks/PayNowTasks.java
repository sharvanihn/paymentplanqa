package com.myntra.pps.tasks;


import com.jayway.jsonpath.JsonPath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.MyntraService;
import com.myntra.lordoftherings.gandalf.PayloadType;
import com.myntra.lordoftherings.gandalf.RequestGenerator;
import com.myntra.monk.core.XMetaRequestContext;
import com.myntra.pps.Constants.PpsConstants;
import com.myntra.pps.apiTests.APINAME;
import com.myntra.pps.apiTests.Myntra;
import com.myntra.pps.apiTests.ServiceType;
import com.myntra.pps.util.TimeUtil;
import org.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;

public class PayNowTasks {
    static Initialize init = new Initialize("/Data/configuration");
    private static final Logger LOGGER = LoggerFactory.getLogger(PayNowTasks.class);
    InstrumentTasks instrumentTasks = new InstrumentTasks();

    public String makePayment(String uidx, String accessToken, String cartId, String cartAmount, String pm, String xMyntCtx,
                                String xid, String dataSource, Boolean isV1, Boolean autoGC, String autoGCAmount,
                                Boolean manualGC, String manualGCAmount, String gcNumber, String gcPin)
            throws JSONException {
        Map<String, Object> addressDetails = getAddressId(xMyntCtx);
        String csrfToken = paymentOptions(accessToken, isV1);

        boolean bankcashback = false;
        String icb = null;
        if(pm.equalsIgnoreCase("creditcard")) {
            icb = instrumentTasks.icbEligibility(xMyntCtx, cartAmount, PpsConstants.card_number, cartId, uidx);
            if (!"NO".equalsIgnoreCase(icb)) {
                bankcashback = true;
            }
        }
        String payload = createPayload(addressDetails, pm, csrfToken, accessToken, cartId, cartAmount, autoGC,
                autoGCAmount,manualGC, manualGCAmount,bankcashback,icb,gcNumber, gcPin);

        String response = getPaynowResponse(payload, isV1, addressDetails, pm, csrfToken, accessToken, cartId, xid, autoGC,
                autoGCAmount, manualGC, manualGCAmount,bankcashback, icb, gcNumber, gcPin);
        Document doc = Jsoup.parse(response);
        Assert.assertNotEquals(doc.title(), "Login", "Something went wrong in pps paynow. Please check pps logs for more details\n\n");
        if(pm.equalsIgnoreCase("creditcard"))
        {
            String cardInfoPayload = extractCardInfoDetails (response);
            // Hit test pg response API
            response = getPGResponsenew(xid,csrfToken,cardInfoPayload);
            doc = Jsoup.parse(response);
        }
        LOGGER.debug("Order status is = " + doc.title() + "\n\n");
        String orderId = doc.select(".msg-num strong").get(0).text().replace("-", "").trim();
        LOGGER.debug("OrderId is = " + orderId + "\n\n");
        validatePpsState(orderId, dataSource);
        return orderId;
    }

    private Map<String, Object> getAddressId(String xMyntCtx) {

        MyntraService addressServiceV2 = Myntra.getService(ServiceType.PORTAL_ADDRESS, APINAME.GETALLADDRESSV2, init.Configurations);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(XMetaRequestContext.X_META_CTX_HEADER, xMyntCtx);
        RequestGenerator response = new RequestGenerator(addressServiceV2, headers);
        Map<String, Object> addressDetails = new HashMap<>();
        addressDetails.put("id", response.respvalidate.NodeValue("data.id", true));
        addressDetails.put("name", response.respvalidate.NodeValue("data.name", true).replace("\"", ""));
        addressDetails.put("address", response.respvalidate.NodeValue("data.address", true).replace("\"", ""));
        addressDetails.put("city", response.respvalidate.NodeValue("data.city", true).replace("\"", ""));
        addressDetails.put("state", response.respvalidate.NodeValue("data.stateCode", true).replace("\"", ""));
        addressDetails.put("country", response.respvalidate.NodeValue("data.countryCode", true).replace("\"", ""));
        addressDetails.put("pincode", response.respvalidate.NodeValue("data.pincode", true).replace("\"", ""));
        addressDetails.put("email", response.respvalidate.NodeValue("data.email", true).replace("\"", ""));
        addressDetails.put("mobile", response.respvalidate.NodeValue("data.mobile", true).replace("\"", ""));

        return addressDetails;
    }

    private String paymentOptions(String accessToken, Boolean isV1){
        HashMap<String, String> header = new HashMap<>();
        header.put(PpsConstants.CLIENT_HEADER, PpsConstants.CLIENT);
        header.put(PpsConstants.VERSION_HEADER, PpsConstants.VERSION);
        header.put(PpsConstants.AUTHORISATION_HEADER, "b92ddbba0e8fb7e8fb1745a0055c5627def81d12037b15a8d8dd8e6963dbc902");
        header.put(PpsConstants.AT_HEADER, accessToken);
        MyntraService service = Myntra.getService(ServiceType.PORTAL_PAYMENTSERVICE, APINAME.PAYMENTOPTIONS,
                init.Configurations, PayloadType.JSON, new String[] { "creditcard,debitcard,savedcard,emi,netbanking,upi,wallet" }, PayloadType.JSON);
        RequestGenerator response = new RequestGenerator(service,header);
        Assert.assertEquals(response.response.getStatus(), 200,
                "Payments paymentOption API is not working\n");
        String csrf = JsonPath.read(response.respvalidate.returnresponseasstring(), "$.csrfToken").toString();
        String encodedCsrf = csrf;
        if(isV1) {
        try {
            encodedCsrf = URLEncoder.encode(csrf, "UTF-8");
        } catch (UnsupportedEncodingException ignored) {
            // Can be safely ignored because UTF-8 is always supported
        }
        }
        return encodedCsrf;
    }

    private String getPaynowResponse(String payload, Boolean isV1, Map<String, Object> addressDetails, String pm, String csrfToken,
                                     String accessToken, String cartId, String xid, Boolean autoGc, String autoGCAmount,
                                     Boolean manualGc, String manualGCAmount, boolean bankcashback, String icbAmount,
                                     String gcNumber, String gcPin) {
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
        HashMap<String,String> hm = new HashMap<>();
        hm.put("Accept", "text/html,application/json,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        hm.put("cookie", "fox-xid=" + xid);
        hm.put("Accept-Language", "en-US,en;q=0.5");
        hm.put("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:50.0) Gecko/20100101 Firefox/50.0");
        hm.put("Content-Type", "application/x-www-form-urlencoded ; charset=UTF-8");
        MyntraService service_PayNowForm = null;
        String str = null;
        if(isV1){
            service_PayNowForm = Myntra.getService(ServiceType.PORTAL_PPS, APINAME.PAYNOWFORM, init.Configurations,
                    new String[] { payload }, PayloadType.URLENCODED, PayloadType.HTML);
            RequestGenerator response = new RequestGenerator (service_PayNowForm,hm);
            Assert.assertEquals(response.response.getStatus(), 200,
                    "Pps PaynowV1 API is not working\n");
            str = response.respvalidate.returnresponseasstring();
        }else {
            service_PayNowForm = Myntra.getService(ServiceType.PORTAL_PPS, APINAME.PAYNOWFORMV2, init.Configurations,
                    new String[] { payload }, PayloadType.URLENCODED, PayloadType.HTML);
            str = getPaynowResponseV2(service_PayNowForm.URL, addressDetails, pm, csrfToken, accessToken, cartId, xid, autoGc,
                    autoGCAmount, manualGc, manualGCAmount,bankcashback, icbAmount, gcNumber, gcPin);
        }
        return str;
    }

    private String getPaynowResponseV2(String url,Map<String, Object> addressDetails, String pm, String csrfToken,
                                       String accessToken, String cartId, String xid, Boolean autoGc,
                                       String autoGCAmount, Boolean manualGc, String manualGCAmount,
                                       boolean bankcashback, String icbAmount, String gcNumber, String gcPin) {
        RequestSpecification requestSpec = given()
                .header("Accept", "text/html,application/json,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                .header("cookie", "fox-xid=" + xid)
                .header("Accept-Language", "en-US,en;q=0.5")
                .header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:50.0) Gecko/20100101 Firefox/50.0")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .formParam("wallet_enabled", PpsConstants.wallet_enabled.trim())
                .formParam("pm", pm.trim())
                .formParam("address", addressDetails.get("id").toString())
                .formParam("clientCode", PpsConstants.clientCode)
                .formParam("cartContext", PpsConstants.cartContext)
                .formParam("profile", PpsConstants.profile)
                .formParam("userGroup", PpsConstants.userGroup)
                .formParam("csrf_token", csrfToken)
                .formParam("email", addressDetails.get("email").toString())
                .formParam("mobile", addressDetails.get("mobile").toString())
                .formParam("b_firstname", addressDetails.get("name").toString())
                .formParam("b_address", addressDetails.get("address").toString())
                .formParam("b_city", addressDetails.get("city").toString())
                .formParam("b_state", addressDetails.get("state").toString())
                .formParam("b_zipcode", addressDetails.get("pincode").toString())
                .formParam("b_country", addressDetails.get("country").toString())
                .formParam("at", accessToken)
                .formParam("auto_giftcard_used", PpsConstants.auto_giftcard_used)
                .formParam("giftcard_type", PpsConstants.giftcard_type)
                .formParam("cartId", cartId);
        if(pm.equalsIgnoreCase("creditcard")){
            requestSpec.formParam("other_cards", PpsConstants.other_cards)
                    .formParam("card_number", PpsConstants.card_number)
                    .formParam("bill_name", PpsConstants.bill_name)
                    .formParam("card_month", PpsConstants.card_month)
                    .formParam("card_year", PpsConstants.card_year)
                    .formParam("cvv_code", PpsConstants.cvv_code)
                    .formParam("address-sel", addressDetails.get("id").toString())
                    .formParam("use_saved_card", PpsConstants.useSavedCard);
        }
        if (autoGc) {
            requestSpec.formParam("auto_giftcard_used", PpsConstants.auto_giftcard_used)
                    .formParam("auto_giftcard_amount", autoGCAmount);
        }
        if (manualGc) {
            requestSpec.formParam("giftcard.0.number", gcNumber)
                    .formParam("giftcard.0.pin", gcPin)
                    .formParam("giftcard.0.amount", manualGCAmount)
                    .formParam("giftcard.count", "1");
        }
        if(bankcashback){
            requestSpec.formParam("bank_cashback_amount", icbAmount)
                    .formParam("bank_cashback_eligible", "true");
        }
        Response response = requestSpec.request().post(url);
        Assert.assertEquals(response.getStatusCode(), 200,
                "Pps paynowV2 API is not working\n");
        return response.asString();
        }

    private String createPayload(Map<String, Object> addressDetails, String pm, String csrfToken,
                                 String accessToken, String cartId, String cartAmount,
                                 boolean autoGc, String autoGCAmount, boolean manualGc,
                                 String manualGCAmount, boolean bankcashback, String icbAmount,
                                 String gcNumber, String gcPin) {

        StringBuffer encodedURL = new StringBuffer();
        if (pm.equalsIgnoreCase("wallet")){
            encodedURL.append("wallet=").append(PpsConstants.wallet).append("&");
            encodedURL.append("wallet_amount=").append(cartAmount).append("&");
            encodedURL.append("wallet_mobile=").append(PpsConstants.wallet_mobile).append("&");
            }
        encodedURL.append("wallet_enabled=").append(PpsConstants.wallet_enabled.trim()).append("&");
        encodedURL.append("pm=").append(pm.trim()).append("&");
        encodedURL.append("address=").append(addressDetails.get("id").toString()).append("&");
        encodedURL.append("clientCode=").append(PpsConstants.clientCode).append("&");
        encodedURL.append("cartContext=").append(PpsConstants.cartContext).append("&");
        encodedURL.append("profile=").append(PpsConstants.profile).append("&");
        encodedURL.append("userGroup=").append(PpsConstants.userGroup).append("&");
        encodedURL.append("csrf_token=").append(csrfToken).append("&");
        encodedURL.append("email=").append(addressDetails.get("email").toString()).append("&");
        encodedURL.append("mobile=").append(addressDetails.get("mobile").toString()).append("&");
        encodedURL.append("b_firstname=").append(addressDetails.get("name").toString()).append("&");
        encodedURL.append("b_address=").append(addressDetails.get("address").toString()).append("&");
        encodedURL.append("b_city=").append(addressDetails.get("city").toString()).append("&");
        encodedURL.append("b_state=").append(addressDetails.get("state").toString()).append("&");
        encodedURL.append("b_zipcode=").append(addressDetails.get("pincode").toString()).append("&");
        encodedURL.append("b_country=").append(addressDetails.get("country").toString()).append("&");
        encodedURL.append("at=").append(accessToken).append("&");
        encodedURL.append("auto_giftcard_used=").append(PpsConstants.auto_giftcard_used).append("&");
        encodedURL.append("giftcard_type=").append(PpsConstants.giftcard_type).append("&");
        encodedURL.append("cartId=").append(cartId);
        if (pm.equalsIgnoreCase("creditcard")) {
            encodedURL.append("&");
            encodedURL.append("other_cards=").append(PpsConstants.other_cards).append("&");
            encodedURL.append("card_number=").append(PpsConstants.card_number).append("&");
            encodedURL.append("bill_name=").append(PpsConstants.bill_name).append("&");
            encodedURL.append("card_month=").append(PpsConstants.card_month).append("&");
            encodedURL.append("card_year=").append(PpsConstants.card_year).append("&");
            encodedURL.append("cvv_code=").append(PpsConstants.cvv_code).append("&");
            encodedURL.append("address-sel=").append(addressDetails.get("id").toString()).append("&");
            encodedURL.append("use_saved_card=").append(PpsConstants.useSavedCard);
        }
        if (bankcashback) {
            encodedURL.append("&");
            encodedURL.append("bank_cashback_amount=").append(icbAmount).append("&");
            encodedURL.append("bank_cashback_eligible=").append("true");
        }
        if (autoGc) {
            encodedURL.append("&");
            encodedURL.append("auto_giftcard_used=").append(PpsConstants.auto_giftcard_used).append("&");
            encodedURL.append("auto_giftcard_amount=").append(autoGCAmount);
        }
        if (manualGc) {
            encodedURL.append("&");
            encodedURL.append("giftcard.0.number=").append(gcNumber).append("&");
            encodedURL.append("giftcard.0.pin=").append(gcPin).append("&");
            encodedURL.append("giftcard.0.amount=").append(manualGCAmount).append("&");
            encodedURL.append("giftcard.count=").append("1");
        }

        String payload = encodedURL.toString();
        System.out.println("Payload is : " + payload);
        return payload;
    }

    private String extractCardInfoDetails(String responseN)
    {
        Document doc1 = Jsoup.parseBodyFragment(responseN);
        Element body1 = doc1.body();

        Element amount = body1.select("input[name=vpc_Amount]").first();
        String vpc_Amount = amount.attr("value").toString();
        System.out.println("amount="+vpc_Amount);

        Element info = body1.select("input[name=vpc_OrderInfo]").first();
        String vpc_orderInfo = info.attr("value").toString();
        System.out.println("amount="+vpc_orderInfo);

        Element message = body1.select("input[name=vpc_Message]").first();
        String vpc_Message = message.attr("value").toString();
        System.out.println("amount="+vpc_Message);

        Element desc = body1.select("input[name=vpc_Desc]").first();
        String vpc_Desc = desc.attr("value").toString();
        System.out.println("amount="+vpc_Desc);

        Element url = body1.select("input[name=vpc_ReturnURL]").first();
        String vpc_ReturnURL = url.attr("value").toString();
        System.out.println("amount="+vpc_ReturnURL);

        Element hash = body1.select("input[name=vpc_SecureHash]").first();
        String vpc_SecureHash = hash.attr("value").toString();
        System.out.println("amount="+vpc_SecureHash);

        Element cardNum = body1.select("input[name=vpc_CardNum]").first();
        String vpc_CardNum = cardNum.attr("value").toString();
        System.out.println("amount="+vpc_CardNum);

        Element authorized = body1.select("input[name=vpc_AuthorizeId]").first();
        String vpc_AuthorizeId = authorized.attr("value").toString();
        System.out.println("amount="+vpc_AuthorizeId);

        Element reference = body1.select("input[name=vpc_MerchTxnRef]").first();
        String vpc_MerchTxnRef = reference.attr("value").toString();
        System.out.println("amount="+vpc_MerchTxnRef);

        Element receipt = body1.select("input[name=vpc_ReceiptNo]").first();
        String vpc_ReceiptNo = receipt.attr("value").toString();
        System.out.println("amount="+vpc_ReceiptNo);

        Element expiry = body1.select("input[name=vpc_CardExp]").first();
        String vpc_CardExp = expiry.attr("value").toString();
        System.out.println("amount="+vpc_CardExp);

        Element code = body1.select("input[name=vpc_CardSecurityCode]").first();
        String vpc_CardSecurityCode = code.attr("value").toString();
        System.out.println("amount="+vpc_CardSecurityCode);

        Element card = body1.select("input[name=vpc_card]").first();
        String vpc_card = card.attr("value").toString();
        System.out.println("amount="+vpc_card);

        Element responseCode = body1.select("input[name=vpc_TxnResponseCode]").first();
        String vpc_TxnResponseCode = responseCode.attr("value").toString();
        System.out.println("amount="+vpc_TxnResponseCode);

        String cardInfoPayload = createPayloadCardInfo(vpc_Amount,vpc_orderInfo,vpc_Message,vpc_Desc,vpc_ReturnURL,vpc_SecureHash,vpc_CardNum,vpc_AuthorizeId,
                vpc_MerchTxnRef,vpc_ReceiptNo,vpc_CardExp,vpc_CardSecurityCode,vpc_card,vpc_TxnResponseCode);

        return cardInfoPayload;

    }


    private String createPayloadCardInfo(String vpc_Amount,String vpc_orderInfo,String vpc_Message,String vpc_Desc,
                                         String vpc_ReturnURL,String vpc_SecureHash,String vpc_CardNum,String vpc_AuthorizeId,
                                         String vpc_MerchTxnRef,String vpc_ReceiptNo,String vpc_CardExp,String vpc_CardSecurityCode,
                                         String vpc_card, String vpc_TxnResponseCode) {
        StringBuffer encodedURL = new StringBuffer();
        encodedURL.append("vpc_Amount=");encodedURL.append(vpc_Amount);encodedURL.append("&");
        encodedURL.append("vpc_orderInfo=");encodedURL.append(vpc_orderInfo.trim());encodedURL.append("&");
        encodedURL.append("vpc_Message=");encodedURL.append(vpc_Message.trim());encodedURL.append("&");
        encodedURL.append("vpc_Desc=");encodedURL.append(vpc_Desc.trim());encodedURL.append("&");
        encodedURL.append("vpc_ReturnURL=");encodedURL.append(vpc_ReturnURL.trim());encodedURL.append("&");
        encodedURL.append("vpc_SecureHash=");encodedURL.append(vpc_SecureHash.trim());encodedURL.append("&");
        encodedURL.append("vpc_CardNum=");encodedURL.append(vpc_CardNum.trim());encodedURL.append("&");
        encodedURL.append("vpc_AuthorizeId=");encodedURL.append(vpc_AuthorizeId.trim());encodedURL.append("&");
        encodedURL.append("vpc_MerchTxnRef=");encodedURL.append(vpc_MerchTxnRef.trim());encodedURL.append("&");
        encodedURL.append("vpc_ReceiptNo=");encodedURL.append(vpc_ReceiptNo.trim());encodedURL.append("&");
        encodedURL.append("vpc_CardExp=");encodedURL.append(vpc_CardExp.trim());encodedURL.append("&");
        encodedURL.append("vpc_CardSecurityCode=");encodedURL.append(vpc_CardSecurityCode.trim());encodedURL.append("&");
        encodedURL.append("vpc_card=");encodedURL.append(vpc_card.trim());encodedURL.append("&");
        encodedURL.append("vpc_TxnResponseCode=");encodedURL.append(vpc_TxnResponseCode.trim());

        String payload = encodedURL.toString();
        System.out.println(payload);
        return payload;
    }

    public String getPGResponsenew(String xid, String sxid,String cardInfoPayload) {
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
        HashMap<String,String>hmx = new HashMap<String,String>();
        String valueCard = "fox-xid="+xid;
        hmx.put("cookie", valueCard);
        hmx.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        hmx.put("Accept-Language", "en-US,en;q=0.5");
        hmx.put("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:50.0) Gecko/20100101 Firefox/50.0");
        hmx.put("Content-Type", "application/x-www-form-urlencoded ; charset=UTF-8");
        hmx.put("csrf_token", sxid);
        MyntraService serviceCard = Myntra.getService(ServiceType.PORTAL_PPS, APINAME.PGRESPONSE, init.Configurations,
                new String[] { cardInfoPayload }, PayloadType.URLENCODED, PayloadType.HTML);
        RequestGenerator reqCard = new RequestGenerator (serviceCard,hmx);
        Assert.assertEquals(reqCard.response.getStatus(), 200,
                "Pps callback API is not working\n");
        String str = reqCard.respvalidate.returnresponseasstring();
        return str;
    }

    public String validatePpsState(String orderID, String dataSource){
        TimeUtil.explicitWait(PpsConstants.WAIT_TIME_FOR_ORDERCONFIRMATON); //Wait time for async flow to complete execution
        Map<String, Object> payment_planTable = DBUtilities.exSelectQueryForSingleRecord("select id, state from payment_plan where orderid = " + orderID, dataSource);
        LOGGER.debug("PpsId is = {}", payment_planTable.get("id"));
        Assert.assertEquals(payment_planTable.get("state"), "PPFSM Order Taking done",  "PpsId(" + payment_planTable.get("id") +") is not in success state. Please check log for any error\n\n");
        return (String) payment_planTable.get("id");
    }
}
