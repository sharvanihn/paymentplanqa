package com.myntra.pps.tasks;

import com.jayway.jsonpath.JsonPath;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.lordoftherings.gandalf.MyntraService;
import com.myntra.lordoftherings.gandalf.RequestGenerator;
import com.myntra.monk.core.MonkUtils;
import com.myntra.monk.core.XMetaRequestContext;
import com.myntra.pps.apiTests.APINAME;
import com.myntra.pps.apiTests.Myntra;
import com.myntra.pps.apiTests.ServiceType;
import com.myntra.pps.data.StyleData;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.util.HashMap;
import java.util.List;

public class AbsolutTasks {
    APIUtilities apiUtil = new APIUtilities();
    static Initialize init = new Initialize("./Data/configuration");
    private static final Logger LOGGER = LoggerFactory.getLogger(AbsolutTasks.class);


    public String[] createCart(String xMyntCtx, String uidx, List<StyleData> styles) {

        // Clear Cart
        clearCartUsingSecuredApi(xMyntCtx, uidx);
        RequestGenerator fetchCart = getCart(xMyntCtx);
        Assert.assertEquals(fetchCart.respvalidate.NodeValue("cartCount", true),
                "0" ,"Items are not cleared from cart\n");

        int totalcartCount = 0;
        for (StyleData style : styles) {
            // Add Item to cart
            RequestGenerator addItemToCartReqGen = addItemToCart(xMyntCtx, style.getStyleId(), style.getSkuId(), String.valueOf(style.getQty()));
            totalcartCount = totalcartCount + style.getQty();
            Assert.assertEquals(addItemToCartReqGen.response.getStatus(), 200,
                    "addItemToCart API is not working\n");

            // Validate Item added
            RequestGenerator getCartReqGen = getCart(xMyntCtx);
            Assert.assertEquals(doesSkuIDExists(getCartReqGen, style.getSkuId()), "Exists",
                    "SkuId " + style.getSkuId() + " is not added in cart");
            Assert.assertEquals(getCartReqGen.respvalidate.NodeValue("cartCount", true),
                    String.valueOf(totalcartCount), "Item is not added to cart");
            Assert.assertEquals(getCartReqGen.respvalidate.NodeValue("products.quantity", true),
                    String.valueOf(style.getQty()), "Item quantity is wrong");
        }
        return getCartData(xMyntCtx);
    }

    public String getXMetaCtxData(String storeId, String nidx, String uidx) {
        HashMap<String, String> xMyntCtxMap = new HashMap<>();
        xMyntCtxMap.put(XMetaRequestContext.X_META_CTX_STORE_ID, storeId);
        xMyntCtxMap.put(XMetaRequestContext.X_META_CTX_NIDX, nidx);
        xMyntCtxMap.put(XMetaRequestContext.X_META_CTX_UIDX, uidx);
        String xMyntCtx = MonkUtils.generateXMetaString(xMyntCtxMap);
        return xMyntCtx;
    }

    public void clearCartUsingSecuredApi(String xMyntCtx, String uidx) {
        MyntraService service = Myntra.getService(ServiceType.PORTAL_CART,
                APINAME.SECUREDAPICLEARCARTV2, init.Configurations);
        service.URL = apiUtil.prepareparameterizedURL(service.URL, uidx);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(XMetaRequestContext.X_META_CTX_HEADER, xMyntCtx);
        RequestGenerator clearCartReqGen = new RequestGenerator(service, headers);
        Assert.assertEquals(clearCartReqGen.response.getStatus(), 200,
                "CheckoutService clearCart API is not working\n");
        getCart(xMyntCtx);
        }

    public RequestGenerator getCart(String xMyntCtx) {
        MyntraService service = Myntra.getService(ServiceType.PORTAL_CART,
                APINAME.OPERATIONFETCHCARTV2, init.Configurations);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(XMetaRequestContext.X_META_CTX_HEADER, xMyntCtx);
        RequestGenerator fetchCart = new RequestGenerator(service, headers);
        return fetchCart;
    }

    public String[] getCartV1(String nidx) {
        MyntraService service = Myntra.getService(ServiceType.PORTAL_CART,
                APINAME.OPERATIONFETCHCART, init.Configurations);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("xid", nidx);
        RequestGenerator fetchCart = new RequestGenerator(service, headers);
        String getCartResponse = fetchCart.respvalidate.returnresponseasstring();
        String[] cartData = {JsonPath.read(getCartResponse, "$.data[0].cartID").toString(),
                JsonPath.read(getCartResponse, "$.data[0].totalPrice").toString()};
        return cartData;
    }

    public String[] getCartData(String xMyntCtx) {
        RequestGenerator getCartReqGen = getCart(xMyntCtx);
        String getCartResponse=getCartReqGen.respvalidate.returnresponseasstring();
        String[] cartData = {JsonPath.read(getCartResponse, "$.id").toString(), JsonPath.read(getCartResponse, "$.price.price").toString()};
        return cartData;
    }


    public RequestGenerator addItemToCart(String xMyntCtx, String styleId, String skuId, String quantity) {
        MyntraService service = Myntra.getService(ServiceType.PORTAL_CART,
                APINAME.ADDITEMTOCARTV2, init.Configurations, new String[] {skuId, styleId, quantity});
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(XMetaRequestContext.X_META_CTX_HEADER, xMyntCtx);
        return new RequestGenerator(service, headers);
    }

    public String doesSkuIDExists(RequestGenerator reqGen, String pSKUId) {
        if (null != pSKUId) {

            List<Integer> selectedSKUIds = JsonPath.read(
                    reqGen.respvalidate.returnresponseasstring(),
                    "$.products[*].skuId");

            if (!CollectionUtils.isEmpty(selectedSKUIds)) {
                for (Integer selectedSKUId : selectedSKUIds) {
                    System.out.println("selectedSKUId:::::"+selectedSKUId);
                    if (selectedSKUId == Integer.parseInt(pSKUId)) {
                        return "Exists";
                    }
                }
            }
            return "Not Exists";
        }

        return "Null SkuID Sent";
    }

    public void applyLoyaltyPoints(String xMyntCtx) {
        MyntraService service = Myntra.getService(ServiceType.PORTAL_CART,
                APINAME.APPLYLOYALTYPOINTSV2, init.Configurations);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(XMetaRequestContext.X_META_CTX_HEADER, xMyntCtx);
        // Apply Loyalty Points
        RequestGenerator applyLoyaltyPointsReqGen = new RequestGenerator(service, headers);
        Assert.assertEquals(applyLoyaltyPointsReqGen.response.getStatus(), 200,
                "CheckoutService applyLoyaltyPoints API is not working\n");
        // Validate Loyalty points are applied
        RequestGenerator getCartReqGen = getCart(xMyntCtx);
        Assert.assertEquals(getCartReqGen.respvalidate.NodeValue("useLoyaltyPoints", true),
                "true", "Loyalty Points Not Applied\n");
    }

    public void applyMyntraCredit(String xMyntCtx) {
        MyntraService service = Myntra.getService(ServiceType.PORTAL_CART, APINAME.APPLYMYNTCREDITV2, init.Configurations);
        HashMap<String, String> headers = new HashMap<>();
        headers.put(XMetaRequestContext.X_META_CTX_HEADER, xMyntCtx);
        RequestGenerator applyMyntCreditReqGen = new RequestGenerator(service, headers);
        Assert.assertEquals(applyMyntCreditReqGen.response.getStatus(), 200,
                "CheckoutService applyMyntCredit API is not working\n");

    }

    public void applyMynts(String xMyntCtx, String couponType, String couponId) {
        MyntraService service = Myntra.getService(ServiceType.PORTAL_CART,
                APINAME.APPLYCOUPONV2, init.Configurations,new String[] {couponType, couponId});
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(XMetaRequestContext.X_META_CTX_HEADER, xMyntCtx);
        RequestGenerator applyMyntReqGen = new RequestGenerator(service, headers);
        Assert.assertEquals(applyMyntReqGen.response.getStatus(), 200,
                "Apply mynt cart api is throwing error\n");
        }
}
