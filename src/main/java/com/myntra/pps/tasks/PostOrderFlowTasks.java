package com.myntra.pps.tasks;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.boromir.DBUtilities;
import com.myntra.lordoftherings.gandalf.MyntraService;
import com.myntra.lordoftherings.gandalf.PayloadType;
import com.myntra.lordoftherings.gandalf.RequestGenerator;
import com.myntra.monk.core.XMetaRequestContext;
import com.myntra.paymentplan.domain.*;
import com.myntra.paymentplan.domain.paymentplan.PaymentPlanItemEntry;
import com.myntra.paymentplan.domain.request.ExchangeOrderRequest;
import com.myntra.paymentplan.domain.request.RefundRequest;
import com.myntra.paymentplan.domain.response.PaymentPlanResponse;
import com.myntra.paymentplan.enums.PPSActionType;
import com.myntra.paymentplan.enums.PPSItemType;
import com.myntra.pps.Constants.PpsConstants;
import com.myntra.pps.apiTests.APINAME;
import com.myntra.pps.apiTests.Myntra;
import com.myntra.pps.apiTests.ServiceType;
import com.myntra.pps.util.AuthUtil;
import com.myntra.pps.util.TimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;


@Slf4j
public class PostOrderFlowTasks {

    static Initialize init = new Initialize("/Data/configuration");
    ObjectMapper mapper = new ObjectMapper();

    public void cancelOrder(String orderId, String dataSource, String storeId, boolean isV1){
        String cancelPpsId = cancelOrReturnOrder(orderId,  getPPSId(orderId, dataSource),
                "CANCELLATION", storeId, dataSource, isV1);
        log.debug("\n\nCancelled ppsID is {}\n\n", cancelPpsId);
    }

    public void returnOrder(String orderId, String dataSource, String storeId, boolean isV1){
        String cancelPpsId = cancelOrReturnOrder(orderId,  getPPSId(orderId, dataSource),
                "RETURN", storeId, dataSource, isV1);
        log.debug("\n\nReturned ppsID is {} \n\n", cancelPpsId);
    }

    public String exchangeOrder(String orderId, String dataSource, String dataSource_Oms, String storeId, String uidx, boolean isV1){
        Map<String, Object> exchangeData = changeStatusInOmsDb(orderId, dataSource_Oms);
        ExchangeOrderRequest exchangeOrderRequest = createExchangeRequest((Long) exchangeData.get("order_id_fk"), (Long) exchangeData.get("id"),
                getPPSId(orderId, dataSource), (Integer) exchangeData.get("style_id"), (Integer) exchangeData.get("sku_id"),
                (Integer) exchangeData.get("seller_id"), uidx);
        HashMap<String, String> header = new HashMap<>();
        header.put(PpsConstants.CLIENT_HEADER, PpsConstants.CLIENT);
        header.put(PpsConstants.VERSION_HEADER, PpsConstants.VERSION);
        header.put(XMetaRequestContext.X_META_CTX_STORE_ID, storeId);
        header.put(PpsConstants.X_META_CTX_HEADER, XMetaRequestContext.X_META_CTX_STORE_ID + "=" + storeId + ";");
        Response response = null;
        try {
            header.put(PpsConstants.AUTHORISATION_HEADER, AuthUtil.getAuthorizationHeader(exchangeOrderRequest, PpsConstants.CLIENT, PpsConstants.VERSION));
            MyntraService service = null;
            if(isV1){
                service = Myntra.getService(ServiceType.PORTAL_PPS,
                        APINAME.EXCHANGEORDER, init.Configurations,
                        new String[] { orderId }, PayloadType.JSON, PayloadType.JSON);
            }else {
                service = Myntra.getService(ServiceType.PORTAL_PPS,
                        APINAME.EXCHANGEORDERV2, init.Configurations,
                        new String[] { orderId }, PayloadType.JSON, PayloadType.JSON);
            }
            response = given().contentType(ContentType.JSON).headers(header)
                    .body(mapper.writeValueAsString(exchangeOrderRequest)).when().post(service.URL);

            Assert.assertEquals(response.getStatusCode(), 200,
                    "Pps Exchange API is not working\n\n" + response.asString());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        String statusMessage = JsonPath.read(response.asString(), "$.success").toString();
        Assert.assertEquals(statusMessage, "true", "Cancellation/Return is not success\n");
        String exchangeOrderId = JsonPath.read(response.asString(), "$.OrderId").toString();
        TimeUtil.explicitWait(PpsConstants.WAIT_TIME_FOR_CANCELLATION);
        return exchangeOrderId;

    }

    private String cancelOrReturnOrder(String orderId,String ppsId,String actionType, String storeId, String dataSource, boolean isV1)
    {
        MyntraService service = null;
        Response response = null;
        HashMap<String, String> header = new HashMap<>();
        header.put(PpsConstants.CLIENT_HEADER, PpsConstants.CLIENT);
        header.put(PpsConstants.VERSION_HEADER, PpsConstants.VERSION);
        header.put(PpsConstants.X_META_CTX_HEADER, storeId);
        RefundRequest refundRequest = createRefundRequest(orderId, ppsId, actionType, dataSource);
        if(actionType!=null && actionType.equalsIgnoreCase("CANCELLATION"))
        {
            if(isV1){
                service = Myntra.getService(ServiceType.PORTAL_PPS,
                        APINAME.ORDERCANCEL, init.Configurations,
                        new String[] { orderId + "_1",ppsId,actionType,orderId}, PayloadType.JSON,
                        PayloadType.JSON);
            }else {
                service = Myntra.getService(ServiceType.PORTAL_PPS,
                        APINAME.ORDERCANCELV2, init.Configurations,
                        new String[] { orderId + "_1",ppsId,actionType,orderId}, PayloadType.JSON,
                        PayloadType.JSON);
            }

        }
        else if(actionType!=null && actionType.equalsIgnoreCase("RETURN"))
        {
            refundRequest.setReturnId(orderId+"_123456_1");
            if (isV1){
                service = Myntra.getService(ServiceType.PORTAL_PPS,
                        APINAME.ORDERRETURN, init.Configurations,
                        new String[] { orderId + "_1",ppsId,actionType,orderId,orderId+"_123456_1"}, PayloadType.JSON,
                        PayloadType.JSON);
            }else {
                service = Myntra.getService(ServiceType.PORTAL_PPS,
                        APINAME.ORDERRETURNV2, init.Configurations,
                        new String[] { orderId + "_1",ppsId,actionType,orderId,orderId+"_123456_1"}, PayloadType.JSON,
                        PayloadType.JSON);
            }

        }
        try {
            header.put(PpsConstants.AUTHORISATION_HEADER, AuthUtil.getAuthorizationHeader(refundRequest, PpsConstants.CLIENT, PpsConstants.VERSION));
            response = given().contentType(ContentType.JSON).headers(header)
                    .body(mapper.writeValueAsString(refundRequest)).when().post(service.URL);
            Assert.assertEquals(response.getStatusCode(), 200,
                    "Pps Cancel/Return API is not working\n");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }

        String statusMessage = JsonPath.read(response.asString(), "$.success").toString();
        Assert.assertEquals(statusMessage, "true", "Cancellation/Return is not success\n");
        String ppsid = JsonPath.read(response.asString(), "$.ppsId").toString();
        TimeUtil.explicitWait(PpsConstants.WAIT_TIME_FOR_CANCELLATION);
        return ppsid;
    }

    public RefundRequest createRefundRequest(String orderId,String ppsId, String actionType, String dataSource){
        RefundRequest refundRequest = new RefundRequest();
        refundRequest.setOrderId(orderId);
        refundRequest.setPpsId(ppsId);
        refundRequest.setTxRefId(orderId + "_1");
        if(actionType!=null && actionType.equalsIgnoreCase("CANCELLATION")){
            refundRequest.setPpsActionType(PPSActionType.CANCELLATION);
        }
        else if(actionType!=null && actionType.equalsIgnoreCase("RETURN")){
            refundRequest.setPpsActionType(PPSActionType.RETURN);
        }


        RequestGenerator paymentPlanReqGen = getPaymentPlan(orderId, dataSource);
        String paymentplan = paymentPlanReqGen.respvalidate.returnresponseasstring();
        PaymentPlanResponse paymentPlanResponse = null;
        try {
            paymentPlanResponse = mapper.readValue(paymentplan, PaymentPlanResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        RefundItem refundItem = new RefundItem();
        List<ItemDetail> items = new ArrayList<>();
        for (PaymentPlanItemEntry PPItem : paymentPlanResponse.getPaymentPlanEntry().getPaymentPlanItems()) {
            ItemDetail itemDetailSku = new ItemDetail();
            itemDetailSku.setType(PPItem.getItemType());
            itemDetailSku.setItemIdentifier(PPItem.getSkuId());
            itemDetailSku.setQuantity(PPItem.getQuantity());
            items.add(itemDetailSku);
        }
        refundItem.setItems(items);
        refundRequest.setRefundItem(refundItem);
        return refundRequest;
    }

    public RequestGenerator getPaymentPlan(String orderId, String dataSource){
        HashMap<String, String> header = new HashMap<>();
        header.put(PpsConstants.CLIENT_HEADER, PpsConstants.CLIENT);
        header.put(PpsConstants.VERSION_HEADER, PpsConstants.VERSION);
        MyntraService service = Myntra.getService(ServiceType.PORTAL_PPS,
                APINAME.GETPAYMENTPLAN, init.Configurations,
                new String[] {getPPSId(orderId, dataSource)}, PayloadType.JSON, PayloadType.JSON);
        try {
            header.put(PpsConstants.AUTHORISATION_HEADER, AuthUtil.getAuthorizationHeader(service.Payload, PpsConstants.CLIENT, PpsConstants.VERSION));
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        RequestGenerator requestGenerator = new RequestGenerator(service, header);
        Assert.assertEquals(requestGenerator.response.getStatus(), 200,
                "PPS GetPaymentPlan API is not working\n");
        return requestGenerator;
    }

    public String getPPSId(String orderID, String dataSource) {
        Map<String, Object> payment_planTable = DBUtilities.exSelectQueryForSingleRecord("select id, state from payment_plan where orderid = " + orderID, dataSource);
        Assert.assertEquals(payment_planTable.get("state"), "PPFSM Order Taking done", "Order is not in success state. Please check log for any error\n");
        return (String) payment_planTable.get("id");
    }

    public Map<String, Object> changeStatusInOmsDb(String orderID, String dataSource){
        Map<String, Object> orderLineTable = DBUtilities.exSelectQueryForSingleRecord("select order_id_fk, id, style_id, sku_id, seller_id from order_line where store_order_id = " + orderID + " limit 1", dataSource);
        Long orderIdFk = (Long) orderLineTable.get("order_id_fk");
        String updateOrderReleaseStatusCode = "UPDATE order_release SET status_code = 'DL', delivered_on=NOW() WHERE order_id_fk='" + orderIdFk + "';";
        String updateOrderLineStatusCode = "UPDATE order_line SET status_code='D' WHERE order_id_fk='" + orderIdFk + "';";
        DBUtilities.exUpdateQuery(updateOrderReleaseStatusCode, dataSource);
        DBUtilities.exUpdateQuery(updateOrderLineStatusCode, dataSource);
        return orderLineTable;
        }

    public ExchangeOrderRequest createExchangeRequest(Long originalOrderId, Long originalOrderLineId, String ppsId,
                                                      Integer styleId, Integer skuId, Integer sellerId, String uidx){
        ExchangeOrderRequest exchangeOrderRequest = new ExchangeOrderRequest();
        exchangeOrderRequest.setOriginalOrderID(originalOrderId.toString());
        exchangeOrderRequest.setOriginalPpsID(ppsId);
        exchangeOrderRequest.setTxRefId("Exchange_" + originalOrderId + "_SKU_" + skuId + "_1");

        ExchangeItem exchangeItem = new ExchangeItem();
        ExchangeItemDetail exchangeItemDetail = new ExchangeItemDetail();
        exchangeItemDetail.setType(PPSItemType.SKU);
        exchangeItemDetail.setItemIdentifier(skuId.toString());
        exchangeItemDetail.setQuantity(1);
        exchangeItemDetail.setSellerId(sellerId.toString());
        exchangeItemDetail.setOptionId(skuId.toString());
        exchangeItemDetail.setStyleId(styleId.toString());
        exchangeItemDetail.setSupplyType("ON_HOLD");
        exchangeItem.setOriginal(exchangeItemDetail);
        exchangeItem.setExchange(exchangeItemDetail);
        exchangeItem.setOriginalOrderLineId(originalOrderLineId.toString());
        exchangeItem.setReasonCode("STL");
        List<ExchangeItem> items = new ArrayList<>();
        items.add(exchangeItem);
        exchangeOrderRequest.setItems(items);

        ShippingAddress shippingAddress = new ShippingAddress();
        shippingAddress.setAddressId("23085323");
        shippingAddress.setReceiverName("pps test name");
        shippingAddress.setShippingAddress("Myntra Design Pvt. Ltd.");
        shippingAddress.setShippingLocality("Singasandra");
        shippingAddress.setShippingCity("Bangalore");
        shippingAddress.setShippingState("KA");
        shippingAddress.setShippingCountry("India");
        shippingAddress.setShippingZipcode("560068");
        shippingAddress.setReceiverMobile("4564564564");
        shippingAddress.setReceiverEmail("autotestv2@myntra.com");
        exchangeOrderRequest.setShippingAddress(shippingAddress);

        exchangeOrderRequest.setUserComments("Exchange Request");
        exchangeOrderRequest.setUserLogin(uidx);
        exchangeOrderRequest.setReasonCode("STL");

        return exchangeOrderRequest;
    }
}
