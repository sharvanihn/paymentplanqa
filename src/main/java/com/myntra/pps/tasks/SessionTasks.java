package com.myntra.pps.tasks;

import com.jayway.jsonpath.JsonPath;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.gandalf.MyntraService;
import com.myntra.lordoftherings.gandalf.RequestGenerator;
import com.myntra.pps.Constants.PpsConstants;
import com.myntra.pps.apiTests.APINAME;
import com.myntra.pps.apiTests.Myntra;
import com.myntra.pps.apiTests.ServiceType;
import com.myntra.pps.data.Tokens;
import com.myntra.pps.util.TokenParser;
import org.testng.Assert;

import java.util.HashMap;

public class SessionTasks {
    static Initialize init = new Initialize("/Data/configuration");

    public Tokens setTokens(String email, String password, String clientId){
        Tokens tokens = new Tokens();
        String at = knuthCreateToken(email, password, clientId);
        tokens.setAccesToken(at);

        TokenParser tokenParser = new TokenParser();
        String[] parsedFromAT = tokenParser.parseTokenAndGetUidxNNidx(tokens.getAccesToken());
        tokens.setUidx(parsedFromAT[0]);
        tokens.setNidx(parsedFromAT[1]);
        return tokens;
    }

    public String knuthCreateToken(String email,String password, String clientId){
        MyntraService token = Myntra.getService(ServiceType.PORTAL_KNUTH, APINAME.KNUTHCREATETOKEN, init.Configurations);
        HashMap<String, String> header = new HashMap<>();
        header.put(PpsConstants.CLIENTID_HEADER, clientId);
        RequestGenerator createToken = new RequestGenerator(token, header);
        Assert.assertEquals(createToken.response.getStatus(), 200,
                "Knuth token API is not working\n");
        String at =  createToken.response.getHeaders().get("at").toString().replace("[", "").replace("]", "");

        MyntraService login = Myntra.getService(ServiceType.PORTAL_KNUTH, APINAME.KNUTHLOGIN, init.Configurations,new String[] { email, password });
        HashMap<String, String> header2 = new HashMap<>();
        header2.put("at", at);
        RequestGenerator createLogin = new RequestGenerator(login, header2);
        Assert.assertEquals(createLogin.response.getStatus(), 200,
                "Knuth login API is not working\n");
        String accessToken = createLogin.response.getHeaders().get("at").toString().replace("[", "").replace("]", "");
        return accessToken;
    }

    // Extract xid=nidx from idp login API
    private String getNidx(String email,String password, String clientId)
    {
        MyntraService login = Myntra.getService(ServiceType.PORTAL_IDP, APINAME.SIGNIN, init.Configurations,new String[] { email, password });
        HashMap<String, String> header = new HashMap<>();
        header.put(PpsConstants.CLIENTID_HEADER, clientId);
        RequestGenerator signin = new RequestGenerator(login);
        return signin.response.getHeaders().get("xid").toString().replace("[", "").replace("]", "");
    }

    // Get accessToken and uidx from idea service
    private String[] getAccessToken(String email,String password, String nidx, String clientId){
        MyntraService ideaSignInUsingEmailService = Myntra.getService(ServiceType.PORTAL_IDEA, APINAME.IDEASIGNINUSINGEMAIL, init.Configurations,
                new String[]{ "Web", password, email } );
        HashMap<String, String> header = new HashMap<>();
        header.put(PpsConstants.CLIENTID_HEADER, clientId);
        header.put(PpsConstants.NIDX_HEADER, nidx);
        RequestGenerator authEmail = new RequestGenerator(ideaSignInUsingEmailService,header);
        Assert.assertEquals(authEmail.response.getStatus(), 200);
        String authEmailResponse = authEmail.respvalidate.returnresponseasstring();
        Assert.assertFalse(JsonPath.read(authEmailResponse, "$.status.statusType").toString().equalsIgnoreCase("error"),
                " Error from idea service. error message is \"" + JsonPath.read(authEmailResponse, "$.status.statusMessage").toString()
                        + "\"\n");
        String uidx=JsonPath.read(authEmailResponse, "$.entry.uidx").toString();
        String at=JsonPath.read(authEmailResponse, "$.tokenEntry.at").toString();
        String[] result = {uidx, at};
        return result;
    }
}
