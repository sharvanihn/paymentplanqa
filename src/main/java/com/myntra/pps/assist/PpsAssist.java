package com.myntra.pps.assist;


import com.myntra.pps.Constants.PpsConstants;
import com.myntra.pps.data.StyleData;
import com.myntra.pps.data.Tokens;
import com.myntra.pps.tasks.*;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

import java.util.List;

public @Data class PpsAssist {

    PayNowTasks payNowTasks = new PayNowTasks();
    AbsolutTasks absolutTasks = new AbsolutTasks();
    SessionTasks sessionTasks = new SessionTasks();
    PostOrderFlowTasks postOrderFlowTasks = new PostOrderFlowTasks();
    InstrumentTasks instrument = new InstrumentTasks();
    private static final Logger LOGGER = LoggerFactory.getLogger(PpsAssist.class);

    /**
     * This method is to create cash on delivery order using pps v2 api's
     *
     * @param email of the user
     * @param password of the user
     * @param clientId "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61" or "jabong-a6aafa2f-ed53-4cb0-8db0-46f8c94f99b5";
     * @param storeId 2297 or 4603
     * @param styles
     * @return orderId
     */
    public String createCodOrder(String email, String password, String clientId, String storeId,
                                 List<StyleData> styles, String dataSource, Boolean isV1, String gcNumber, String gcPin){
        Tokens tokens = sessionTasks.setTokens(email, password, clientId);
        String xMyntCtx = absolutTasks.getXMetaCtxData(storeId, tokens.getNidx(), tokens.getUidx());
        String[] cartData = absolutTasks.createCart(xMyntCtx, tokens.getUidx(), styles);
        return payNowTasks.makePayment(tokens.getUidx(), tokens.getAccesToken(), cartData[0], cartData[1],"cod",
                xMyntCtx, tokens.getNidx(), dataSource,  isV1, false, "0", false, "0",
                gcNumber, gcPin);
        }

    public String createWalletOrder(String email,String password, String clientId, String storeId,
                                    List<StyleData> styles, String dataSource, Boolean isV1, String gcNumber, String gcPin) {
        Tokens tokens = sessionTasks.setTokens(email, password, clientId);
        String xMyntCtx = absolutTasks.getXMetaCtxData(storeId, tokens.getNidx(), tokens.getUidx());
        String[] cartData = absolutTasks.createCart(xMyntCtx, tokens.getUidx(), styles);
        return payNowTasks.makePayment(tokens.getUidx(), tokens.getAccesToken(), cartData[0], cartData[1], "wallet",
                xMyntCtx, tokens.getNidx(), dataSource, isV1, false, "0", false, "0",
                gcNumber, gcPin);
    }

    public String createOnlineOrder(String email, String password, String clientId, String storeId,
                                    List<StyleData> styles, String dataSource, Boolean isV1, String gcNumber, String gcPin){
        Tokens tokens = sessionTasks.setTokens(email, password, clientId);
        String xMyntCtx = absolutTasks.getXMetaCtxData(storeId, tokens.getNidx(), tokens.getUidx());
        absolutTasks.createCart(xMyntCtx, tokens.getUidx(),styles);
        String[] cartData = absolutTasks.getCartV1(tokens.getNidx());
        return payNowTasks.makePayment(tokens.getUidx(), tokens.getAccesToken(), cartData[0], cartData[1],"creditcard",
                xMyntCtx, tokens.getNidx(), dataSource, isV1, false, "0", false, "0",
                gcNumber, gcPin);
    }

    public String createCompleteAgcOrder(String email, String password, String clientId, String storeId,
                                          List<StyleData> styles, String dataSource, Boolean isV1, String gcNumber, String gcPin){
        Tokens tokens = sessionTasks.setTokens(email, password, clientId);
        String xMyntCtx = absolutTasks.getXMetaCtxData(storeId, tokens.getNidx(), tokens.getUidx());
        //Prepare cart
        absolutTasks.createCart(xMyntCtx, tokens.getUidx(),styles);
        String[] cartData = absolutTasks.getCartV1(tokens.getNidx());
        Long amount = Math.round(Double.valueOf(cartData[1])+1);
        instrument.matchGiftCardBalance(xMyntCtx, tokens.getUidx(), amount);
        absolutTasks.applyMyntraCredit(xMyntCtx);
        cartData = absolutTasks.getCartV1(tokens.getNidx());
        return payNowTasks.makePayment(tokens.getUidx(), tokens.getAccesToken(), cartData[0], cartData[1],"cashback",
                xMyntCtx, tokens.getNidx(), dataSource, isV1, true, cartData[1], false, "0",
                gcNumber, gcPin);
    }

    public String createCompleteMgcOrder(String email, String password, String clientId, String storeId,
                                         List<StyleData> styles, String dataSource, Boolean isV1, String gcNumber, String gcPin){
        Tokens tokens = sessionTasks.setTokens(email, password, clientId);
        String xMyntCtx = absolutTasks.getXMetaCtxData(storeId, tokens.getNidx(), tokens.getUidx());
        //Prepare cart
        absolutTasks.createCart(xMyntCtx, tokens.getUidx(),styles);
        String[] cartData = absolutTasks.getCartV1(tokens.getNidx());
        String mgcBalance = instrument.getGiftcardBalance(xMyntCtx, tokens.getUidx(), gcNumber, gcPin);
        return payNowTasks.makePayment(tokens.getUidx(), tokens.getAccesToken(), cartData[0], cartData[1],"giftcard",
                xMyntCtx, tokens.getNidx(), dataSource, isV1, false, "0", true, cartData[1],
                gcNumber, gcPin);
    }

    public String createLpAgcMyntIcbOnlineOrder(String email, String password, String clientId, String storeId,
                                                List<StyleData> styles, String dataSource, Boolean isV1, String gcNumber, String gcPin){
        Tokens tokens = sessionTasks.setTokens(email, password, clientId);
        String xMyntCtx = absolutTasks.getXMetaCtxData(storeId, tokens.getNidx(), tokens.getUidx());
        instrument.checkLoyaltyPoints(xMyntCtx, tokens.getUidx());
        //Prepare cart
        absolutTasks.createCart(xMyntCtx, tokens.getUidx(),styles);
        String[] cartData = absolutTasks.getCartV1(tokens.getNidx());
        Long amount = Math.round(Double.valueOf(cartData[1])) - Math.round(Double.valueOf(cartData[1])/2);
        String autogcBalance = instrument.matchGiftCardBalance(xMyntCtx, tokens.getUidx(), amount);
        while(Math.round(Double.valueOf(autogcBalance)) > Math.round(Double.valueOf(cartData[1]))){
            for (StyleData style : styles) {
                style.setQty(style.getQty() + 1);
            }
        }
        absolutTasks.applyMynts(xMyntCtx, PpsConstants.MYNT_TYPE, PpsConstants.MYNT_CODE);
        absolutTasks.applyMyntraCredit(xMyntCtx);
        absolutTasks.applyLoyaltyPoints(xMyntCtx);
        cartData = absolutTasks.getCartV1(tokens.getNidx());
        return payNowTasks.makePayment(tokens.getUidx(), tokens.getAccesToken(), cartData[0], cartData[1],"creditcard",
                xMyntCtx, tokens.getNidx(), dataSource, isV1, false, "0", false, "0",
                gcNumber, gcPin);
    }

    public String createMgcAndCodOrder(String email, String password, String clientId, String storeId,
                                          List<StyleData> styles, String dataSource, Boolean isV1, String gcNumber, String gcPin){
        Tokens tokens = sessionTasks.setTokens(email, password, clientId);
        String xMyntCtx = absolutTasks.getXMetaCtxData(storeId, tokens.getNidx(), tokens.getUidx());
        //Prepare cart
        absolutTasks.createCart(xMyntCtx, tokens.getUidx(),styles);
        String[] cartData = absolutTasks.getCartV1(tokens.getNidx());
        String mgcBalance = instrument.getGiftcardBalance(xMyntCtx, tokens.getUidx(), gcNumber, gcPin);
        return payNowTasks.makePayment(tokens.getUidx(), tokens.getAccesToken(), cartData[0], cartData[1],"cod",
                xMyntCtx, tokens.getNidx(), dataSource, isV1, false, "0", true, mgcBalance,
                gcNumber, gcPin);
    }

    public void createLpAgcMyntIcbOnlineOrder_exchange(String email, String password, String clientId, String storeId,
                                                         List<StyleData> styles, String dataSource, Boolean isV1, String dataSource_Oms,
                                                       String gcNumber, String gcPin){
        Tokens tokens = sessionTasks.setTokens(email, password, clientId);
        String xMyntCtx = absolutTasks.getXMetaCtxData(storeId, tokens.getNidx(), tokens.getUidx());
        instrument.checkLoyaltyPoints(xMyntCtx, tokens.getUidx());
        //Prepare cart
        absolutTasks.createCart(xMyntCtx, tokens.getUidx(),styles);
        String[] cartData = absolutTasks.getCartV1(tokens.getNidx());
        Long amount = Math.round(Double.valueOf(cartData[1])) - Math.round(Double.valueOf(cartData[1])/2);
        String autogcBalance = instrument.matchGiftCardBalance(xMyntCtx, tokens.getUidx(), amount);
        while(Math.round(Double.valueOf(autogcBalance)) > Math.round(Double.valueOf(cartData[1]))){
            for (StyleData style : styles) {
                style.setQty(style.getQty() + 1);
            }
        }
        absolutTasks.applyMynts(xMyntCtx, PpsConstants.MYNT_TYPE, PpsConstants.MYNT_CODE);
        absolutTasks.applyMyntraCredit(xMyntCtx);
        absolutTasks.applyLoyaltyPoints(xMyntCtx);
        cartData = absolutTasks.getCartV1(tokens.getNidx());
        String orderId = payNowTasks.makePayment(tokens.getUidx(), tokens.getAccesToken(), cartData[0], cartData[1],"creditcard",
                xMyntCtx, tokens.getNidx(), dataSource, isV1, false, "0", false, "0",
                gcNumber, gcPin);
        Reporter.log(orderId);
        exchangeOrder(orderId, dataSource, dataSource_Oms, storeId, tokens.getUidx(), isV1);
    }

    /**
     * This method is to cancel order using pps v2 api's
     *
     * @param orderId
     * @param dataSource = pps we are now using pps2
     * @param storeId storeId=2297; or storeId=4603;
     * @return cancelled ppsId
     */
    public void cancelOrder(String orderId, String dataSource, String storeId, boolean isV1){
        postOrderFlowTasks.cancelOrder(orderId, dataSource, storeId, isV1);
    }

    public void returnOrder(String orderId, String dataSource, String storeId, boolean isV1){
        postOrderFlowTasks.returnOrder(orderId, dataSource, storeId, isV1);
    }

    public void exchangeOrder(String orderId, String dataSource, String dataSource_Oms, String storeId, String uidx, boolean isV1){
        postOrderFlowTasks.exchangeOrder(orderId, dataSource, dataSource_Oms, storeId, uidx, isV1);
    }

    public String createLpAndOnlineOrder(String email, String password, String clientId, String storeId,
                                         List<StyleData> styles, String dataSource, Boolean isV1, String gcNumber, String gcPin){
        Tokens tokens = sessionTasks.setTokens(email, password, clientId);
        String xMyntCtx = absolutTasks.getXMetaCtxData(storeId, tokens.getNidx(), tokens.getUidx());
        instrument.checkLoyaltyPoints(xMyntCtx, tokens.getUidx());
        //Prepare cart
        absolutTasks.createCart(xMyntCtx, tokens.getUidx(),styles);
        absolutTasks.applyLoyaltyPoints(xMyntCtx);
        String[] cartData = absolutTasks.getCartV1(tokens.getNidx());
        return payNowTasks.makePayment(tokens.getUidx(), tokens.getAccesToken(), cartData[0], cartData[1],"creditcard",
                xMyntCtx, tokens.getNidx(), dataSource, isV1, false, "0", false, "0",
                gcNumber, gcPin);
    }

    public String createAgcAndOnlineOrder(String email, String password, String clientId, String storeId,
                                          List<StyleData> styles, String dataSource, Boolean isV1, String gcNumber, String gcPin){
        Tokens tokens = sessionTasks.setTokens(email, password, clientId);
        String xMyntCtx = absolutTasks.getXMetaCtxData(storeId, tokens.getNidx(), tokens.getUidx());
        //Prepare cart
        absolutTasks.createCart(xMyntCtx, tokens.getUidx(),styles);
        String[] cartData = absolutTasks.getCartV1(tokens.getNidx());
        Long amount = Math.round(Double.valueOf(cartData[1])) - Math.round(Double.valueOf(cartData[1])/2);
        String autogcBalance = instrument.matchGiftCardBalance(xMyntCtx, tokens.getUidx(), amount);
        while(Math.round(Double.valueOf(autogcBalance)) > Math.round(Double.valueOf(cartData[1]))){
            for (StyleData style : styles) {
                style.setQty(style.getQty() + 1);
            }
        }
        absolutTasks.applyMyntraCredit(xMyntCtx);
        cartData = absolutTasks.getCartV1(tokens.getNidx());
        return payNowTasks.makePayment(tokens.getUidx(), tokens.getAccesToken(), cartData[0], cartData[1],"creditcard",
                xMyntCtx, tokens.getNidx(), dataSource, isV1, true, autogcBalance, false, "0",
                gcNumber, gcPin);
    }

    public String createMgcAndOnlineOrder(String email, String password, String clientId, String storeId,
                                          List<StyleData> styles, String dataSource, Boolean isV1, String gcNumber, String gcPin){
        Tokens tokens = sessionTasks.setTokens(email, password, clientId);
        String xMyntCtx = absolutTasks.getXMetaCtxData(storeId, tokens.getNidx(), tokens.getUidx());
        //Prepare cart
        absolutTasks.createCart(xMyntCtx, tokens.getUidx(),styles);
        String[] cartData = absolutTasks.getCartV1(tokens.getNidx());
        String mgcBalance = instrument.getGiftcardBalance(xMyntCtx, tokens.getUidx(), gcNumber, gcPin);
        return payNowTasks.makePayment(tokens.getUidx(), tokens.getAccesToken(), cartData[0], cartData[1],"creditcard",
                xMyntCtx, tokens.getNidx(), dataSource, isV1, false, "0", true, mgcBalance,
                gcNumber, gcPin);
    }

    public String createMyntAndOnlineOrder(String email, String password, String clientId, String storeId,
                                           List<StyleData> styles, String dataSource, Boolean isV1, String gcNumber, String gcPin){
        Tokens tokens = sessionTasks.setTokens(email, password, clientId);
        String xMyntCtx = absolutTasks.getXMetaCtxData(storeId, tokens.getNidx(), tokens.getUidx());

        //Prepare cart
        absolutTasks.createCart(xMyntCtx, tokens.getUidx(),styles);
        String[] cartData = absolutTasks.getCartV1(tokens.getNidx());
        absolutTasks.applyMynts(xMyntCtx, PpsConstants.MYNT_TYPE, PpsConstants.MYNT_CODE);

        return payNowTasks.makePayment(tokens.getUidx(), tokens.getAccesToken(), cartData[0], cartData[1],"creditcard",
                xMyntCtx, tokens.getNidx(), dataSource, isV1, false, "0", false, "0",
                gcNumber, gcPin);
    }

}
