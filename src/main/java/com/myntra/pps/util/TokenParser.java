package com.myntra.pps.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.myntra.idea.client.IdeaClientnd;
import com.myntra.idea.entry.KeysEntry;
import com.myntra.idea.token.sdk.*;
import com.myntra.idea.token.sdk.config.Config;
import com.myntra.idea.token.sdk.model.Token;
import com.myntra.idea.token.sdk.util.JWTTokenTransformer;
import com.myntra.idea.token.sdk.util.RSAKeyUtils;
import com.myntra.monk.core.XMetaRequestContext;
import com.myntra.paymentplan.enums.PPSErrors;
import com.myntra.paymentplan.exception.PaymentPlanException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.security.interfaces.RSAPublicKey;


public class TokenParser {
    private static final String KEY_VALUE_SEPARATOR = "=";
    private static final String RECORD_SEPARATOR=";";
    private static final Logger LOGGER = LoggerFactory.getLogger(TokenParser.class);
    private TokenProcessor tokenProcessor;
    private Config config;


    public void init(){
        String env = "stage";
        if(null!=System.getenv("environment")){
            env = System.getenv("environment");
        }
        String ideaUrl = "http://idea." + env + ".myntra.com";
        IdeaClientnd ideaClient = new IdeaClientnd(ideaUrl);
        KeyCache keyCache = new KeyCacheImpl(ideaClient);
        this.tokenProcessor = new TokenProcessorImpl(keyCache);
    }

    public String parseTokenAndGetXMyntHeader(String at){
        if(StringUtils.isEmpty(at))
            throw new PaymentPlanException(PPSErrors.INVALID_AT_TOKEN);

        Token token = parseToken(at);
        return verifyAndGenerateXMyntHeaderFromToken(token);
    }

    public String[] parseTokenAndGetUidxNNidx(String at){
        if(StringUtils.isEmpty(at))
            throw new PaymentPlanException(PPSErrors.INVALID_AT_TOKEN);

        Token token = parseToken(at);
        return verifyAndGenerateUidxNNidxFromToken(token);
    }



    public Token parseToken(String at) {
        try{
            init();
            Token token = tokenProcessor.parseWithVerification(at);
            LOGGER.debug("Token details after parsing the at token {}",token);
            return token;
        } catch (TokenException te){
            LOGGER.error("Exception while parsing the at token {}",at,te);
            throw new PaymentPlanException(PPSErrors.INVALID_AT_TOKEN);
        }
    }



    public String verifyAndGenerateXMyntHeaderFromToken(Token token){
        if(StringUtils.isEmpty(token.getClaims().getStoreId())){
            LOGGER.error("Invalid StoreId after parsing at token {}",token.getClaims().toString());
        }

        if(StringUtils.isEmpty(token.getClaims().getUidx())){
            LOGGER.error("Invalid Uidx after parsing at token {}",token.getClaims().toString());
        }

        if(StringUtils.isEmpty(token.getClaims().getNidx())){
            LOGGER.error("Invalid Nidx after parsing at token {}",token.getClaims().toString());
        }
        StringBuilder headerValue = new StringBuilder();
        headerValue.append(XMetaRequestContext.X_META_CTX_STORE_ID).append(KEY_VALUE_SEPARATOR)
                .append(token.getClaims().getStoreId() == null ? "" : token.getClaims().getStoreId())
                .append(RECORD_SEPARATOR);
        headerValue.append(XMetaRequestContext.X_META_CTX_UIDX).append(KEY_VALUE_SEPARATOR)
                .append(token.getClaims().getUidx() == null ? "" : token.getClaims().getUidx())
                .append(RECORD_SEPARATOR);
        headerValue.append(XMetaRequestContext.X_META_CTX_NIDX).append(KEY_VALUE_SEPARATOR)
                .append(token.getClaims().getNidx() == null ? "" : token.getClaims().getNidx());
        return headerValue.toString();
    }

    public String[] verifyAndGenerateUidxNNidxFromToken(Token token){
        if(StringUtils.isEmpty(token.getClaims().getUidx())){
            LOGGER.error("Invalid Uidx after parsing at token {}",token.getClaims().toString());
        }
        if(StringUtils.isEmpty(token.getClaims().getNidx())){
            LOGGER.error("Invalid Nidx after parsing at token {}",token.getClaims().toString());
        }
        String[] result = {token.getClaims().getUidx(), token.getClaims().getNidx()};
        return result;
    }

    public Token parseWithVerification(String encodedToken) throws TokenException {
        try {
            if (encodedToken == null) {
                throw new TokenException("token cannot be null");
            } else {
                String decodedToken = new String(Base64.decodeBase64(encodedToken.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
                JWT jwt = JWT.decode(decodedToken);
                return this.verify(jwt);
            }
        } catch (Exception var4) {
            throw new TokenException(var4);
        }
    }

    private Token verify(JWT jwt) throws TokenException {
        try {
            String kid = jwt.getKeyId();
            if (kid == null) {
                throw new TokenException("Jwt token without kid");
            } else {
                KeysEntry keysEntry = new KeysEntry();
                keysEntry.setPublicKey("-----BEGIN PUBLIC KEY-----\n" +
                        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCDWWw3rpn2GeTUhNKiFZNLbahUOJEkYw8EpISHzxWhhWpY1D4Wwg8bpWYKBTAzNz1F3buLPgzv8kxq8oqFQfnnNeEIWN2ENDHyZhV54EM6fsryNoyRISzJqcrK3wOTChw4oJOBzBPKueCbbyO3D68sGpIvxUhmHaUfuBEzoSiosQIDAQAB\n" +
                        "-----END PUBLIC KEY-----");
                if (keysEntry == null) {
                    throw new TokenException("Unknown key with token id: " + kid);
                } else {
                    Algorithm algorithm;
                    if (jwt.getAlgorithm().equals(Token.KeyAlgo.HS256.toString())) {
                        algorithm = Algorithm.HMAC256(keysEntry.getPublicKey());
                    } else {
                        if (!jwt.getAlgorithm().equals(Token.KeyAlgo.RS256.toString())) {
                            throw new TokenException("Token with unknown alog");
                        }

                        RSAPublicKey key = RSAKeyUtils.getPublicKeyFromPEMString(keysEntry.getPublicKey());
                        algorithm = Algorithm.RSA256(key);
                    }

                    JWTVerifier verifier = JWT.require(algorithm).withIssuer("IDEA").build();
                    verifier.verify(jwt.getToken());
                    return JWTTokenTransformer.jwtToToken(jwt);
                }
            }
        } catch (Exception var6) {
            throw new TokenException(var6);
        }
    }
}
