package com.myntra.pps.util;

import com.google.common.base.Predicate;
import org.apache.commons.lang.StringUtils;
import org.reflections.ReflectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.lang.reflect.Method;
import java.util.*;

public class SignatureUtil {

    private static final String SEPERATOR = ".";
    private static final String DELIMITER = "|";
    private static final String GETTER_PREFIX = "get";
    private static final String BOOLEAN_GETTER_PREFIX = "is";
    private static final String MAP_KEY_PREFIX = "key";
    private static final String MAP_VALUE_PREFIX = "value";
    private static final String NATIVE_PACKAGE = "com.myntra";
    static final Logger logger = LoggerFactory.getLogger(SignatureUtil.class);

    public SignatureUtil() {
    }

    public static <T> String generateCheckSum(T requestObject) {
        new TreeMap();

        TreeMap requestDetailsMap;
        try {
            requestDetailsMap = getSortedMapFromRequest(requestObject, "");
        } catch (ClassNotFoundException var3) {
            logger.error("ClassNotFoundException in generating checksum", var3);
            throw new WebApplicationException(Response.Status.FORBIDDEN);
        }

        String checksum = createChecksum(requestDetailsMap);
        return checksum;
    }

    private static String createChecksum(TreeMap<String, Object> requestDetailsMap) {
        StringBuilder sb = new StringBuilder();
        Iterator var2 = requestDetailsMap.keySet().iterator();

        while(var2.hasNext()) {
            String key = (String)var2.next();
            sb.append(requestDetailsMap.get(key)).append("|");
        }

        return sb.substring(0, sb.length() - 1);
    }

    private static <T> TreeMap<String, Object> getSortedMapFromRequest(T obj, String prefix) throws ClassNotFoundException {
        HashMap<String, Object> requestFields = new HashMap();
        Set<Method> getters = ReflectionUtils.getAllMethods(obj.getClass(), new Predicate[]{ReflectionUtils.withModifier(1), ReflectionUtils.withPrefix("get")});
        Set<Method> booleanGetters = ReflectionUtils.getAllMethods(obj.getClass(), new Predicate[]{ReflectionUtils.withModifier(1), ReflectionUtils.withPrefix("is")});
        getters.addAll(booleanGetters);
        Iterator var5 = getters.iterator();

        while(var5.hasNext()) {
            Method getter = (Method)var5.next();

            try {
                String fieldName = getFieldName(getter);
                if (constructSortedMapForClass(getter.getReturnType())) {
                    requestFields.putAll(getSortedMapFromRequest(getter.invoke(obj), prefix + "." + fieldName));
                } else {
                    getter.setAccessible(true);
                    if (getter.invoke(obj) != null) {
                        if (getter.getReturnType().getCanonicalName().equals(Map.class.getCanonicalName())) {
                            HashMap<Object, Object> map = (HashMap)getter.invoke(obj);
                            requestFields.putAll(constructSortedMapForMap(map, prefix, fieldName));
                        } else if (getter.getReturnType().getCanonicalName().equals(List.class.getCanonicalName())) {
                            List<Object> list = (List)getter.invoke(obj);
                            requestFields.putAll(constructSortedMapForList(list, prefix, fieldName));
                        } else {
                            Object methodValue = getter.invoke(obj);
                            if (methodValue != null && (!(methodValue instanceof String) || !StringUtils.isEmpty((String)methodValue))) {
                                requestFields.put(prefix + "." + fieldName, getter.invoke(obj));
                            }
                        }
                    }
                }
            } catch (Exception var9) {
            }
        }

        return new TreeMap(requestFields);
    }

    private static String getFieldName(Method getMethod) {
        return getMethod.getName().startsWith("get") ? getMethod.getName().substring(3, getMethod.getName().length()) : getMethod.getName().substring(2, getMethod.getName().length());
    }

    private static TreeMap<String, Object> constructSortedMapForMap(HashMap<Object, Object> map, String prefix, String fieldName) throws ClassNotFoundException {
        HashMap<String, Object> requestFields = new HashMap();
        boolean constructSortedMapForValue = false;
        boolean constructSortedMapForKey = false;
        if (!map.isEmpty()) {
            if (constructSortedMapForClass(map.values().toArray()[0].getClass())) {
                constructSortedMapForValue = true;
            }

            if (constructSortedMapForClass(map.keySet().toArray()[0].getClass())) {
                constructSortedMapForKey = true;
            }
        }

        String prefixForValue = "";
        String prefixForKey = "";
        int index = 0;

        for(Iterator var9 = map.keySet().iterator(); var9.hasNext(); ++index) {
            Object key = var9.next();
            Object textString = map.get(key);
            prefixForKey = prefix + "." + fieldName + "." + "key" + "." + index;
            if (constructSortedMapForKey) {
                requestFields.putAll(getSortedMapFromRequest(key, prefixForKey));
            } else if (textString != null) {
                requestFields.put(prefixForKey, key);
            }

            prefixForValue = prefix + "." + fieldName + "." + "value" + "." + index;
            if (constructSortedMapForValue) {
                requestFields.putAll(getSortedMapFromRequest(textString, prefixForValue));
            } else if (textString != null) {
                requestFields.put(prefixForValue, textString);
            }
        }

        return new TreeMap(requestFields);
    }

    private static TreeMap<String, Object> constructSortedMapForList(List<Object> list, String prefix, String fieldName) throws ClassNotFoundException {
        HashMap<String, Object> requestFields = new HashMap();
        boolean constructSortedMapForListObject = false;
        if (!list.isEmpty() && constructSortedMapForClass(list.get(0).getClass())) {
            constructSortedMapForListObject = true;
        }

        String prefixForValue = "";

        for(int i = 0; i < list.size(); ++i) {
            prefixForValue = prefix + "." + fieldName + "." + i;
            if (constructSortedMapForListObject) {
                requestFields.putAll(getSortedMapFromRequest(list.get(i), prefixForValue));
            } else {
                Object methodValue = list.get(i);
                if (methodValue != null && (!(methodValue instanceof String) || !StringUtils.isEmpty((String)methodValue))) {
                    requestFields.put(prefixForValue, methodValue);
                }
            }
        }

        return new TreeMap(requestFields);
    }

    private static boolean constructSortedMapForClass(Class<?> className) {
        return className.getCanonicalName().startsWith("com.myntra") && !className.isEnum();
    }
}
