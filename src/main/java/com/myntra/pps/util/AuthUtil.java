package com.myntra.pps.util;

import java.security.NoSuchAlgorithmException;

public class AuthUtil {

    public static String getAuthorizationHeader(Object request, String client, String version) throws NoSuchAlgorithmException {
        String payload = SignatureUtil.generateCheckSum(request);
        String payloadString = getPayloadString(payload, client, version);
        return HashingUtils.generateSignature(payloadString, "qa");
    }

    private static String getPayloadString(String objectString, String client, String version) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(client);
        stringBuilder.append("|");
        stringBuilder.append(version);
        stringBuilder.append("|");
        stringBuilder.append(objectString);
        return stringBuilder.toString();
    }
}
