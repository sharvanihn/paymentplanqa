package com.myntra.pps.util;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashingUtils {

    private static final String CHECKSUM_DELIMITER = "|";

    private static final String HASH_TYPE = "SHA-256";

    final static Logger logger = LoggerFactory.getLogger(HashingUtils.class);

    public static String computeSHA256(String data) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(HASH_TYPE);
        byte[] hashBytes = md.digest(data.getBytes());
        return Hex.encodeHexString(hashBytes);
    }

    public static String generateSignature(String objectString, String key) throws NoSuchAlgorithmException {
        try {
            return computeSHA256(objectString + CHECKSUM_DELIMITER + key);
        } catch (Exception e) {
            logger.error("exception in generating signature", e);
        }
        return null;
    }

}
