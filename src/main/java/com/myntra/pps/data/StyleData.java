package com.myntra.pps.data;

import lombok.Data;

public @Data class StyleData {
    private String styleId;
    private String skuId;
    private int qty;

}
