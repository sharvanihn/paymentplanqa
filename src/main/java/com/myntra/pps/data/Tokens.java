package com.myntra.pps.data;

import lombok.Data;

public @Data class Tokens {
    private String nidx;
    private String uidx;
    private String accesToken;
    private String csrftoken;
}
