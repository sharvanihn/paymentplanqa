package com.myntra.pps.Constants;

public class  PpsConstants {

    public static final String MYNTRA_CLIENTID = "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61";
    public static final String JABONG_CLIENTID = "jabong-a6aafa2f-ed53-4cb0-8db0-46f8c94f99b5";

    public static final String MYNTRA_PPS_DATASOURCE = "pps";
    public static final String JABONG_PPS_DATASOURCE = "jabong_pps";
    public static final String MYNTRA_OMS_DATASOURCE = "myntra_oms";

    public static final String CLIENTID_HEADER = "clientId";
    public static final String NIDX_HEADER = "nidx";
    public static final String CLIENT_HEADER = "client";
    public static final String VERSION_HEADER = "version";
    public static final String AUTHORISATION_HEADER = "Authorization";
    public static final String AT_HEADER = "AT";
    public static final String X_META_CTX_HEADER = "x-mynt-ctx";
    public static final String MYNTRA_X_MYNT_CTX = "storeId=2297;";
    public static final String JABONG_X_MYNT_CTX = "storeId=4603;";

    public static final String CLIENT = "qa";
    public static final String VERSION = "1.0";
    public static final String MYNTRA_STOREID = "2297";
    public static final String JABONG_STOREID = "4603";

    public static final String clientCode = "responsive";
    public static final String cartContext = "DEFAULT";
    public static final String profile = "myntra.com";
    public static final String userGroup = "normal";
    public static final String wallet_enabled = "true";
    public static final String wallet = "paytm";
    public static final String wallet_mobile = "8547761572";
    public static final String other_cards = "false";
    public static final String card_number = "5415679107547290";
    public static final String bill_name = "Test card";
    public static final String card_month = "07";
    public static final String card_year = "22";
    public static final String cvv_code = "123";
    public static final String useSavedCard = "false";
    public static final String auto_giftcard_used = "true";
    public static final String giftcard_type = "autoGiftcard";

    public static final String gcNumber = "6991201053649297";
    public static final String gcPin = "653511";
    public static final String gcType = "GIFTBIG";

    public static final String MYNT_TYPE = "mynts";
    public static final String MYNT_CODE = "AUTOT10P";

    public static final int WAIT_TIME_FOR_ORDERCONFIRMATON = 3000;
    public static final int WAIT_TIME_FOR_GOODWILLPURCHASE = 3000;
    public static final int WAIT_TIME_FOR_CANCELLATION = 8000;
}
